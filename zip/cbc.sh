#!/system/xbin/bash

#    Copyright 2018 Rafal Wisniewski (wisniew99@gmail.com)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


#########################
# Cherry Bash Commander #
#          0.1          #
#########################

#
# Variables
#

VERSION="0.281"
CHANNEL="stable"

#
# Other functions
#

clear_output () {
     echo -e '\0033\0143'
}

#
# Start
#

#TODO Auto update process toggable in options
#TODO Prepering screen with chacking for updates

#
# env variables
#

case $1 in
     "-h" | "--help")

          echo ""
          echo "Cherry Bash Commander licenced under GNU GPL v3"
          echo "by Wisniew"
          echo ""
          echo ""
          echo "To normally use script run it with:"
          echo ""
          echo "bash cbc"
          echo "or"
          echo "./cbc"
          echo ""
          echo ""
          echo "Emergency variables:"
          echo ""
          echo "-fu --force-update     Force update script to latest stable version"
          echo "-fu-dev --force-update-dev     Force update script to latest dev version"
          echo ""
          exit 0

          ;;

     "-fu" | "--force-update")
          
          echo ""
          echo " Downloading..."
          echo ""
          curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc.sh?inline=false"
          echo ""
          echo " Installing and configurating..."
          chmod 755 cbc
          echo ""
          echo " Done!"
          echo ""
          exit 0

          ;;

     "-fu-dev" | "--force-update-dev")

          echo ""
          echo " Downloading..."
          echo ""
          curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc-dev.sh?inline=false"
          echo ""
          echo " Installing and configurating..."
          chmod 755 cbc
          echo ""
          echo " Done!"
          echo ""
          exit 0

          ;;
esac
# env to restore from backup, clear init.d, reset all settings etc...

#
# Menu functions
#

main_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| "
     echo ""
     if [ "$CHANNEL" = "dev" ]
     then
          echo " Cherry Bash Commander DEV"
     elif [ "$CHANNEL" = "stable" ]
     then
          echo " Cherry Bash Commander $VERSION"
     fi
     echo ""
     echo ""
     echo " Select option:"
     echo "  1. Info"
     echo "  2. Monitor"
     echo "  3. Edit"
     echo "  4. Others"
     echo "  5. Profiles (WIP)"
     echo "  6. Updates"
     echo "  7. Settings"
     echo "  8. About"
     echo ""
     echo "  0. Exit"
     echo ""
     read  -n 1 MENUINPUT

     if [ "$MENUINPUT" == 1 ]
     then
          info_menu

     elif [ "$MENUINPUT" == 2 ]
     then
          monitor_menu

     elif [ "$MENUINPUT" == 3 ]
     then
          edit_menu

     elif [ "$MENUINPUT" == 4 ]
     then
          others_menu

     elif [ "$MENUINPUT" == 5 ]
     then
          profiles_menu

     elif [ "$MENUINPUT" == 6 ]
     then
          updates_menu

     elif [ "$MENUINPUT" == 7 ]
     then
          settings_menu

     elif [ "$MENUINPUT" == 8 ]
     then
          about_menu

     elif [ "$MENUINPUT" == 0 ]
     then
          clear_output
          exit 0
     else
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu"
          echo ""
          read -n 1
          main_menu
     fi
}

info_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Informer"
     echo ""
     echo " Select option:"
     echo "  1. Software"
     echo "  2. Hardware"
     echo ""
     echo "  3. Show getprop"
     echo ""
     echo "  b. Return to the main menu"
     echo ""
     read -n 1 MENUINPUT
     if [ $MENUINPUT == 1 ]
     then
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Informer - software"
          echo ""
          echo -n " Kernel version: " && uname -r
          echo -n " Hostname: " && hostname
          if [ ! -z $(getprop ro.build.type) ]
          then
               echo -n " System time: " && getprop ro.build.type
          fi
          if [ ! -z $(getprop ro.build.expect.firmware) ]
          then
               echo -n " Firmware: " && getprop ro.build.expect.firmware
          fi          
          if [ ! -z $(getprop ro.mod.version) ]
          then
               echo -n " ROM name: " && getprop ro.mod.version
          fi
          if [ ! -z $(getprop ro.keymaster.xxx.release) ]
          then
               echo -n " Base version: " && getprop ro.keymaster.xxx.release
          fi
          if [ ! -z $(getprop ro.build.version.release) ]
          then
               echo -n " ROM version: " && getprop ro.build.version.release
          fi
          if [ ! -z $(getprop ro.build.user) ]
          then
               echo -n " Base author: " && getprop ro.build.user
          fi
          if [ ! -z $(getprop ro.mk.maintainer) ]
          then
               echo -n " ROM author: " && getprop ro.mk.maintainer
          fi          
          if [ ! -z $(getprop ro.build.id) ]
          then
               echo -n " ROM ID: " && getprop ro.build.id
          fi
          if [ ! -z $(getprop ro.build.host) ]
          then
               echo -n " ROM host: " && getprop ro.build.host
          fi
          if [ ! -z $(getprop ro.keymaster.xxx.security_patch) ]
          then
               echo -n " ROM secure level: " && getprop ro.keymaster.xxx.security_patch
          fi
          if [ ! -z $(getprop ro.build.selinux) ]
          then
               echo -n " Selinux: " && getprop ro.build.selinux
          fi
          echo ""
          echo ""
          echo " Press any key to return."
          echo ""
          read -n 1
          info_menu
     elif [ $MENUINPUT == 2 ]
     then
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Informer - hardware"
          echo ""
          echo -n " Architecture: " && uname -m
          if [ ! -z $(getprop ro.hardware) ]
          then
               echo -n " Hardware: " && getprop ro.hardware
          fi
          if [ ! -z $(getprop ro.product.device) ]
          then
               echo -n " Device: " && getprop ro.product.device
          fi
          if [ ! -z $(getprop ro.product.vendor.device) ]
          then
               echo -n " Device name: " && getprop ro.product.vendor.device
          fi
          if [ ! -z $(getprop ro.product.manufacturer) ]
          then
               echo -n " Device manufacturer: " && getprop ro.product.manufacturer
          fi
          if [ ! -z $(getprop ro.product.cpu.abi) ]
          then
               echo -n " CPU ABI: " && getprop ro.product.cpu.abi
          fi
          if [ ! -z $(getprop ro.product.board) ]
          then
               echo -n " CPU: " && getprop ro.product.board
          fi
          echo ""
          echo " Press any key to return."
          echo ""
          read -n 1
          info_menu
     elif [ $MENUINPUT == 3 ]
     then
          clear_output
          for i in $(seq 1 50)
          do  
             echo ""
          done
          echo " ################### GETPROP START ###################"
          echo ""
          getprop #TODO Make it looks better
          echo ""
          echo " Press any key to return."
          echo ""
          read -n 1
          info_menu
     elif [ "$MENUINPUT" == "b" ]
     then
          main_menu
     elif [ "$MENUINPUT" == "B" ]
     then
          main_menu
     else
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
     fi
}

monitor_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Monitor"
     echo ""
     echo " Select option:"
     echo "  1. CPU"
     echo "  2. Temperatures"
     echo "  3. RAM"
     echo ""
     echo "  4. htop"
     echo ""
     echo "  b. Return to the main menu"
     echo ""
     read -n 1 MENUINPUT

     if [ "$MENUINPUT" == "1" ]
     then
          NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
          BIG=$((NUMBER/2))
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Monitor - CPU"
          echo ""
          echo " Select cluster:"
          echo "  1. BIG cluster"
          echo "  2. little cluster"
          echo ""
          echo "  b. Return to the main menu"
          echo ""
          read -n 1 MENUINPUT
          if [ "$MENUINPUT" == "1" ]
          then
               while :; do
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Monitor - CPU - BIG cluster"
                    echo ""
                    echo " BIG cluster"
                    echo ""
                    echo " Governor:"
                    echo -n "  Current - " && cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_governor
                    echo ""
                    echo " Frequency:"
                    echo -n "  Current - " && cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_cur_freq
                    echo -n "  Max - " && cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_max_freq
                    echo -n "  Min - " && cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_min_freq
                    echo ""
                    echo "Press any key to return."
                    echo ""
                    MENUINPUT=""
                    read -n 1 -t 1 MENUINPUT
                    if [ ! -z "$MENUINPUT" ]
                    then
                         monitor_menu
                    fi
               done
          elif [ "$MENUINPUT" == "2" ]
          then
               while :; do
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Monitor - CPU - little cluster"
                    echo ""
                    echo " little cluster"
                    echo ""
                    echo " Governor:"
                    echo -n "  Current - " && cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor
                    echo ""
                    echo " Frequency:"
                    echo -n "  Current - " && cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_cur_freq
                    echo -n "  Max - " && cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_max_freq
                    echo -n "  Min - " && cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_min_freq
                    echo ""
                    echo " Press any key to return."
                    echo ""
                    MENUINPUT=""
                    read -n 1 -t 1 MENUINPUT
                    if [ ! -z "$MENUINPUT" ]
                    then
                         monitor_menu
                    fi
               done
          elif [ "$MENUINPUT" == "b" ]
          then
               monitor_menu
          elif [ "$MENUINPUT" == "B" ]
          then
               monitor_menu
          else
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
          fi
     elif [ "$MENUINPUT" == "2" ]
     then
          while :; do
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Monitor - Temperatures"
               echo ""
               number=$(ls -1 /sys/devices/virtual/thermal/ | wc -l)
               number=$((number-31))
               for i in $(seq 1 $number)
               do
                    i=$((i-1))
                    if [ ! "$(cat /sys/devices/virtual/thermal/thermal_zone$i/temp)" == "0" ] || [ ! "$(cat /sys/devices/virtual/thermal/thermal_zone$i/type)" == "tsens_tz_sensor"* ] || [ ! "$(cat /sys/devices/virtual/thermal/thermal_zone$i/type)" == "limits_sensor"* ]
                    then 
                         cat /sys/devices/virtual/thermal/thermal_zone$i/type | tr -d "\n" && echo -n ": " && cat /sys/devices/virtual/thermal/thermal_zone$i/temp | tr -d "\n"
                    fi
                    echo ""
               done
               echo ""
               echo "Press any key to return."
               echo ""
               MENUINPUT=""
               read -n 1 -t 1 MENUINPUT
               if [ ! -z "$MENUINPUT" ]
               then
                    monitor_menu
               fi
          done
     elif [ "$MENUINPUT" == "3" ]
     then
          while :; do
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Monitor - RAM"
               echo ""
               cat /proc/meminfo | grep "MemTotal"
               cat /proc/meminfo | grep "MemFree"
               cat /proc/meminfo | grep "MemAvailable"
               echo ""
               cat /proc/meminfo | grep "Buffers"
               cat /proc/meminfo | grep "Cached" | grep -v "Swap"
               echo ""
               echo "Press any key to return."
               echo ""
               MENUINPUT=""
               read -n 1 -t 1 MENUINPUT
               if [ ! -z "$MENUINPUT" ]
               then
                    monitor_menu
               fi
          done
     elif [ "$MENUINPUT" == "4" ]
     then
          htop
          main_menu
     elif [ "$MENUINPUT" == "b" ]
     then
          main_menu
     elif [ "$MENUINPUT" == "B" ]
     then
          main_menu
     else
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
     fi
}

edit_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Editor"
     echo ""
     echo " Select section to edit:"
     echo "  1. CPU"
     echo "  2. GPU"
     echo "  3. Memory"
     echo "  4. Audio (WIP)"
     echo "  5. Other (WIP)"
     echo ""
     echo "  b. Return to the main menu"
     echo ""
     read -n 1 MENUINPUT

     if [ "$MENUINPUT" == "1" ]
     then
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Editor - CPU"
          echo ""
          echo " Select cluster:"
          echo "  1. BIG cluster"
          echo "  2. little cluster"
          echo ""
          echo "  b. Return to the edit menu"
          read -n 1 MENUINPUT
          if [ "$MENUINPUT" == "1" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - CPU - BIG cluster"
               echo ""
               echo " Select cluster:"
               echo "  1. Governor"
               echo "  2. Max frequency"
               echo "  3. Min frequency"
               echo ""
               echo "  b. Return to the edit menu"
               read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "1" ]
               then
                    NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
                    BIG=$((NUMBER/2))
                    governors=($(cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_available_governors))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - BIG - Governor"
                    echo ""
                    echo -n " Current governor: " && cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_governor
                    echo ""
                    echo " Select governors:"
                    for index in ${!governors[*]}
                    do
                        printf "  %4d. %s\n" $index ${governors[$index]}
                    done
                    echo ""
                    echo " Change value to (b to return):"
                    read -n 1 MENUINPUT
                    if [ "$MENUINPUT" == "b" ]
                    then
                         edit_menu
                    else
                         echo "${governors[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_governor
                         edit_menu
                    fi
               elif [ "$MENUINPUT" == "2" ]
               then
                    NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
                    BIG=$((NUMBER/2))
                    freqs=($(cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_available_frequencies))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - BIG - Max freq"
                    echo ""
                    echo -n " Current frequency: " && cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_max_freq
                    echo ""
                    echo " Select frequency:"
                    for ((i=0; i< "${#freqs[@]}"; i++))
                    do
                         echo -n "  $i. ${freqs[$i]}"
                         i=$((i+1))

                         if (( $i > 10 ))
                         then
                              echo -n "   "
                         else
                              echo -n "    "
                         fi

                         if [ ! "${freqs[$i]}" = "" ]
                         then
                              echo "$i. ${freqs[$i]}"
                         else
                              echo ""
                         fi
                         
                         i=$((i+1))
                    done
                    echo ""
                    echo " Change value to (b to return):"
                    read -n 2 MENUINPUT
                    if [ "$MENUINPUT" == "b" ]
                    then
                         edit_menu
                    else
                         echo "${freqs[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_max_freq
                         edit_menu
                    fi
               elif [ "$MENUINPUT" == "3" ]
               then
                    NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
                    BIG=$((NUMBER/2))
                    freqs=($(cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_available_frequencies))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - BIG - Min freq"
                    echo ""
                    echo -n " Current frequency: " && cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_min_freq
                    echo ""
                    echo " Select frequency:"
                    for ((i=0; i< "${#freqs[@]}"; i++))
                    do
                         echo -n "  $i. ${freqs[$i]}"
                         i=$((i+1))

                         if (( $i > 10 ))
                         then
                              echo -n "   "
                         else
                              echo -n "    "
                         fi

                         if [ ! "${freqs[$i]}" = "" ]
                         then
                              echo "$i. ${freqs[$i]}"
                         else
                              echo ""
                         fi
                         
                         i=$((i+1))
                    done
                    echo ""
                    echo " Change value to (b to return):"
                    read -n 2 MENUINPUT
                    if [ "$MENUINPUT" == "b" ]
                    then
                         edit_menu
                    else
                         echo "${freqs[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_min_freq
                         edit_menu
                    fi
               fi
               if [ "$MENUINPUT" == "b" ]
               then
                    edit_menu
               elif [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    clear_output
                    echo ""
                    echo " ERROR!"
                    echo " You have entered an invalid selection!"
                    echo " Press any key to return to the main menu."
                    echo ""
                    read -n 1
                    main_menu
               fi

          elif [ "$MENUINPUT" == "2" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - CPU - little cluster"
               echo ""
               echo " Select cluster:"
               echo "  1. Governor"
               echo "  2. Max frequency"
               echo "  3. Min frequency"
               echo ""
               echo "  b. Return to the edit menu"
               read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "1" ]
               then
                    governors=($(cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_available_governors))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - little - Governor"
                    echo ""
                    echo -n " Current governor: " && cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor
                    echo ""
                    echo " Select governors:"
                    for index in ${!governors[*]}
                    do
                        printf "  %4d. %s\n" $index ${governors[$index]}
                    done
                    echo ""
                    echo " Change value to (b to return):"
                    read -n 1 MENUINPUT
                    if [ "$MENUINPUT" == "b" ]
                    then
                         edit_menu
                    else
                         echo "${governors[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor
                         edit_menu
                    fi
               elif [ "$MENUINPUT" == "2" ]
               then
                    freqs=($(cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_available_frequencies))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - little - Max freq"
                    echo ""
                    echo -n " Current frequency: " && cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_max_freq
                    echo ""
                    echo " Select frequency:"
                    for ((i=0; i< "${#freqs[@]}"; i++))
                    do
                         echo -n "  $i. ${freqs[$i]}"
                         i=$((i+1))

                         if (( $i > 10 ))
                         then
                              echo -n "   "
                         else
                              echo -n "    "
                         fi

                         if [ ! "${freqs[$i]}" = "" ]
                         then
                              echo "$i. ${freqs[$i]}"
                         else
                              echo ""
                         fi
                         
                         i=$((i+1))
                    done
                    echo ""
                    echo " Change value to (b to return):"
                    read -n 2 MENUINPUT
                    if [ "$MENUINPUT" == "b" ]
                    then
                         edit_menu
                    else
                         echo "${freqs[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_max_freq
                         edit_menu
                    fi
               elif [ "$MENUINPUT" == "3" ]
               then
                    NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
                    BIG=$((NUMBER/2))
                    freqs=($(cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_available_frequencies))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - little - Min freq"
                    echo ""
                    echo -n " Current frequency: " && cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_min_freq
                    echo ""
                    echo " Select frequency:"
                    for ((i=0; i< "${#freqs[@]}"; i++))
                    do
                         echo -n "  $i. ${freqs[$i]}"
                         i=$((i+1))

                         if (( $i > 10 ))
                         then
                              echo -n "   "
                         else
                              echo -n "    "
                         fi

                         if [ ! "${freqs[$i]}" = "" ]
                         then
                              echo "$i. ${freqs[$i]}"
                         else
                              echo ""
                         fi
                         
                         i=$((i+1))
                    done
                    echo ""
                    echo " Change value to (b to return):"
                    read -n 2 MENUINPUT
                    if [ "$MENUINPUT" == "b" ]
                    then
                         edit_menu
                    else
                         echo "${freqs[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_min_freq
                         edit_menu
                    fi
               fi
               if [ "$MENUINPUT" == "b" ]
               then
                    edit_menu
               elif [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    clear_output
                    echo ""
                    echo " ERROR!"
                    echo " You have entered an invalid selection!"
                    echo " Press any key to return to the main menu."
                    echo ""
                    read -n 1
                    main_menu
               fi

          fi

          if [ "$MENUINPUT" == "b" ]
          then
               edit_menu
          elif [ "$MENUINPUT" == "B" ]
          then
               edit_menu
          else
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
          fi

     elif [ "$MENUINPUT" == "2" ]
     then
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Editor - GPU"
          echo ""
          echo " Select section to edit:"
          if [ -e /sys/devices/platform/kcal_ctrl.0/kcal_enable ]
          then
               echo "  1. KCAL"
          fi
          echo ""
          echo "  b. Return to main editor menu"
          echo ""
          read -n 1 MENUINPUT
          if [ -e /sys/devices/platform/kcal_ctrl.0/kcal_enable ]
          then
               if [ "$MENUINPUT" == "1" ]
               then
                    if [ $(cat /sys/devices/platform/kcal_ctrl.0/kcal_enable) = 0 ]
                    then
                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                         echo ""
                         echo " KCAL is disabled!"
                         echo " Do You want to enable? (y/n)"
                         echo ""
                         read -n 1 MENUINPUT
                         if [ "$MENUINPUT" == "y" ]
                         then
                              echo "1" > /sys/devices/platform/kcal_ctrl.0/kcal_enable
                         elif [ "$MENUINPUT" == "n" ]
                         then
                              edit_menu
                         fi
                    else
                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                         echo ""
                         echo " Select option to edit: "
                         echo "  1. Disable KCAL"
                         echo -n "  2. Colors (" && cat /sys/devices/platform/kcal_ctrl.0/kcal | tr -d "\n" && echo ")"
                         echo -n "  3. Contrast ("&& cat /sys/devices/platform/kcal_ctrl.0/kcal_cont | tr -d "\n" && echo ")"
                         echo -n "  4. Saturation ("&& cat /sys/devices/platform/kcal_ctrl.0/kcal_sat | tr -d "\n" && echo ")"
                         echo -n "  5. Minimal value ("&& cat /sys/devices/platform/kcal_ctrl.0/kcal_min | tr -d "\n" && echo ")"
                         echo -n "  6. Hue ("&& cat /sys/devices/platform/kcal_ctrl.0/kcal_hue | tr -d "\n" && echo ")"
                         echo -n "  7. Value ("&& cat /sys/devices/platform/kcal_ctrl.0/kcal_val | tr -d "\n" && echo ")"
                         #echo -n " 8. Colors invert ("&& cat /sys/devices/platform/kcal_ctrl.0/kcal_invert | tr '\n' ' ' && echo ")"
                         echo ""
                         echo "  b. back to edit menu"
                         echo ""
                         read -n 1 MENUINPUT
                         if [ "$MENUINPUT" == "1" ]
                         then
                              echo "0" > sys/devices/platform/kcal_ctrl.0/kcal_enable
                         elif [ "$MENUINPUT" == "2" ]
                         then
                              echo "TODO"
                         elif [ "$MENUINPUT" == "3" ] # Contrast
                         then
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Contrast is set to: " && cat /sys/devices/platform/kcal_ctrl.0/kcal_cont
                              echo " value: 0-610 default: 255" #610 is max???
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ]
                              then
                                   edit_menu
                              else
                                   echo "$MENUINPUT" > /sys/devices/platform/kcal_ctrl.0/kcal_cont
                                   edit_menu
                              fi
                         elif [ "$MENUINPUT" == "4" ] # Saturation
                         then
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Saturation is set to: " && cat /sys/devices/platform/kcal_ctrl.0/kcal_sat
                              echo " value: 0-610 default: 255" #TODO 610 is max??? TEST IT
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ]
                              then
                                   edit_menu
                              else
                                   echo "$MENUINPUT" > /sys/devices/platform/kcal_ctrl.0/kcal_sat
                                   edit_menu
                              fi
                         elif [ "$MENUINPUT" == "5" ] # Minimal
                         then
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Minimal value is set to: " && cat /sys/device/platform/kcal_ctrl.0/kcal_min
                              echo " value: 0-255 default: 35"
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ]
                              then
                                   edit_menu
                              else
                                   echo "$MENUINPUT" > /sys/devices/platform/kcal_ctrl.0/kcal_min
                                   edit_menu
                              fi
                         elif [ "$MENUINPUT" == "6" ] # hue
                         then
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Hue is set to: " && cat /sys/devices/platform/kcal_ctrl.0/kcal_hue
                              echo " value: 0-255 default: 0"
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ]
                              then
                                   edit_menu
                              else
                                   echo "$MENUINPUT" > /sys/device/platform/kcal_ctrl.0/kcal_hue
                                   edit_menu
                              fi
                         elif [ "$MENUINPUT" == "6" ] # value
                         then
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Value is set to: " && cat /sys/devices/platform/kcal_ctrl.0/kcal_val
                              echo " value: 0-610 default: 255" #TODO 610 is max??? TEST IT
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ]
                              then
                                   edit_menu
                              else
                                   echo "$MENUINPUT" > /sys/devices/platform/kcal_ctrl.0/kcal_val
                                   edit_menu
                              fi
                         fi

                         if [ "$MENUINPUT" == "b" ]
                         then
                              edit_menu
                         elif [ "$MENUINPUT" == "B" ]
                         then
                              edit_menu
                         else
                              clear_output
                              echo ""
                              echo " ERROR!"
                              echo " You have entered an invalid selection!"
                              echo " Press any key to return to the main menu."
                              echo ""
                              read -n 1
                              main_menu
                         fi
                    fi
               fi
          fi
          if [ "$MENUINPUT" == "b" ]
          then
               edit_menu
          elif [ "$MENUINPUT" == "B" ]
          then
               edit_menu
          else
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
          fi
     elif [ "$MENUINPUT" == "3" ]
     then
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Editor - Memory"
          echo ""
          echo " Select section to edit:"
          echo -n "  1. ReadAHead (" && cat /sys/block/sda/bdi/read_ahead_kb | tr -d "\n" && echo ")"
          echo -n "  2. Swappiness (" && cat /proc/sys/vm/swappiness | tr -d "\n" && echo ")"
          echo -n "  3. VFS cache pressure (" && cat /proc/sys/vm/vfs_cache_pressure | tr -d "\n" && echo ")"
          echo -n "  4. Dirty ratio (" && cat /proc/sys/vm/dirty_ratio | tr -d "\n" && echo ")"
          echo -n "  5. Dirty background ratio (" && cat /proc/sys/vm/dirty_background_ratio | tr -d "\n" && echo ")"
          echo -n "  6. Extra free kbytes (" && cat /proc/sys/vm/extra_free_kbytes | tr -d "\n" && echo ")"
          echo -n "  7. Laptop mode (" && cat /proc/sys/vm/laptop_mode | tr -d "\n" && echo ")"
          echo ""
          echo "  b. Return to main editor menu"
          echo ""
          read -n 1 MENUINPUT
          if [ "$MENUINPUT" == "1" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - ReadAHead"
               echo ""
               echo -n " ReadAHead is set to: " && cat /sys/block/sda/bdi/read_ahead_kb
               echo " value: 0-8096 default: 128"
               echo ""
               echo -n " Change value to (or b to return):" && read -n 4 MENUINPUT
               if [ "$MENUINPUT" == "b" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /sys/block/sda/bdi/read_ahead_kb
                    edit_menu
               fi
          elif [ "$MENUINPUT" == "2" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - Swappiness"
               echo ""
               echo -n " Swappiness is set to: " && cat /proc/sys/vm/swappiness
               echo " value: 0-200 default: 60"
               echo ""
               echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
               if [ "$MENUINPUT" == "b" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/swappiness
                    edit_menu
               fi
          elif [ "$MENUINPUT" == "3" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - VFS cache pr..."
               echo ""
               echo -n " VFS cache pressure is set to: " && cat /proc/sys/vm/vfs_cache_pressure
               echo " value: 0-9999 default: 100"
               echo ""
               echo -n " Change value to (or b to return):" && read -n 4 MENUINPUT
               if [ "$MENUINPUT" == "b" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/vfs_cache_pressure
                    edit_menu
               fi
          elif [ "$MENUINPUT" == "4" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - Dirty ratio"
               echo ""
               echo -n " Dirty ratio is set to: " && cat /proc/sys/vm/dirty_ratio
               echo " value: 0-9999 default: 20" #TODO to test!
               echo ""
               echo -n " Change value to (or b to return):" && read -n 4 MENUINPUT
               if [ "$MENUINPUT" == "b" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/dirty_ratio
                    edit_menu
               fi
          elif [ "$MENUINPUT" == "5" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - Dirty backgr..."
               echo ""
               echo -n " Dirty background ratio is set to: " && cat /proc/sys/vm/dirty_background_ratio
               echo " value: 0-9999 default: 5" #TODO to test!
               echo ""
               echo -n " Change value to (or b to return):" && read -n 4 MENUINPUT
               if [ "$MENUINPUT" == "b" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/dirty_background_ratio
                    edit_menu
               fi
          elif [ "$MENUINPUT" == "6" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - Extra free k..."
               echo ""
               echo -n " Extra free kbytes is set to: " && cat /proc/sys/vm/extra_free_kbytes
               echo " value: 0-999999" #TODO to test!
               echo ""
               echo -n " Change value to (or b to return):" && read -n 6 MENUINPUT
               if [ "$MENUINPUT" == "b" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/extra_free_kbytes
                    edit_menu
               fi
          elif [ "$MENUINPUT" == "7" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - Laptop mode"
               echo ""
               echo -n " Laptop mode is set to: " && cat /proc/sys/vm/laptop_mode
               echo " value: 0-1 default: 0" #TODO to test!
               echo ""
               echo -n " Change value to (or b to return):" && read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "b" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/laptop_mode
                    edit_menu
               fi
          fi
          if [ "$MENUINPUT" == "b" ]
          then
               edit_menu
          elif [ "$MENUINPUT" == "B" ]
          then
               edit_menu
          else
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
          fi
     elif [ "$MENUINPUT" == "b" ]
     then
          main_menu
     elif [ "$MENUINPUT" == "B" ]
     then
          main_menu
     else
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
     fi
}

others_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Others"
     echo ""
     echo " Select option:"
     echo "  1. init.d"
     echo "  2. Drop caches"
     echo ""
     echo "  b. Return to the main menu"
     echo ""
     read -n 1 MENUINPUT
     if [ "$MENUINPUT" == "1" ]
     then
          if [ -e /etc/init.d/99cbc ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Others - init.d"
               echo ""
               if [ "$(cat /etc/init.d/99cbc)" == "" ]
               then
                    echo " CBC init.d is empty"
               else
                    echo " CBC init.d is present"
               fi
               echo ""
               echo " Select option:"
               echo "  1. View init.d file"
               echo "  2. Push current settings"
               echo "  3. Push settings from profile (WIP)"
               echo "  4. Delete file"
               echo ""
               echo "  b. Return to the settings menu"
               echo ""
               read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "1" ]
               then
                    for i in $(seq 1 50)
                    do  
                       echo ""
                    done
                    echo " ##################### INIT.D FILE #####################"
                    echo ""
                    cat /etc/init.d/99cbc #TODO Make it looks better
                    echo ""
                    echo " Press any key to return."
                    echo ""
                    read -n 1
                    others_menu
               elif [ "$MENUINPUT" == "2" ]
               then
                    NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
                    BIG=$((NUMBER/2))

                    mount -o rw,remount /system
                    rm -rf /etc/init.d/99cbc
                    touch /etc/init.d/99cbc
                    chmod 755 /etc/init.d/99cbc


                    echo "#!/system/bin/sh" >> /etc/init.d/99cbc
                    echo "CBC init.d / profile file" >> /etc/init.d/99cbc
                    echo "Script is autogenerated by CBC $VERSION" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc

                    echo "CPU:" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "BIG cluster" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_governor)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_governor" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_max_freq)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_max_freq" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_min_freq)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_min_freq" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "little cluster" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_max_freq)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_max_freq" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/bus/cpu/devices/cpu0/cpufreq/scaling_min_freq)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_min_freq" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc

                    echo "GPU" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "KCAL:" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/devices/platform/kcal_ctrl.0/kcal_enable)
                    echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_enable" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/devices/platform/kcal_ctrl.0/kcal_cont)
                    echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_cont" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/devices/platform/kcal_ctrl.0/kcal_sat)
                    echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_sat" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/devices/platform/kcal_ctrl.0/kcal_min)
                    echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_min" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/devices/platform/kcal_ctrl.0/kcal_hue)
                    echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_hue" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/devices/platform/kcal_ctrl.0/kcal_val)
                    echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_val" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc

                    echo "Memory:" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    CURRENT=$(cat /sys/block/sda/bdi/read_ahead_kb)
                    echo "echo \"$CURRENT\" > /sys/block/sda/bdi/read_ahead_kb" >> /etc/init.d/99cbc
                    CURRENT=$(cat /proc/sys/vm/swappiness)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/swappiness" >> /etc/init.d/99cbc
                    CURRENT=$(cat /proc/sys/vm/vfs_cache_pressure)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/vfs_cache_pressure" >> /etc/init.d/99cbc
                    CURRENT=$(cat /proc/sys/vm/dirty_ratio)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/dirty_ratio" >> /etc/init.d/99cbc
                    CURRENT=$(cat /proc/sys/vm/dirty_background_ratio)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/dirty_background_ratio" >> /etc/init.d/99cbc
                    CURRENT=$(cat /proc/sys/vm/extra_free_kbytes)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/extra_free_kbytes" >> /etc/init.d/99cbc
                    CURRENT=$(cat /proc/sys/vm/laptop_mode)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/laptop_mode" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc

                    echo "Audio:" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc

                    echo "Others:" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc


                    mount -o ro,remount /system
                    others_menu
               elif [ "$MENUINPUT" == "3" ]
               then
                    others_menu
               elif [ "$MENUINPUT" == "4" ]
               then
                    mount -o rw,remount /system
                    rm -rf /etc/init.d/99cbc
                    mount -o ro,remount /system
                    others_menu
               fi
               if [ "$MENUINPUT" == "b" ]
               then
                    others_menu
               elif [ "$MENUINPUT" == "B" ]
               then
                    others_menu
               else
                    clear_output
                    echo ""
                    echo " ERROR!"
                    echo " You have entered an invalid selection!"
                    echo " Press any key to return to the main menu."
                    echo ""
                    read -n 1
                    main_menu
               fi
          else
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Others - init.d"
               echo ""
               echo " CBC init.d file not exist!"
               echo ""
               echo " Do You want to create that file (y/n)?"
               echo ""
               read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "y" ]
               then
                    mount -o rw,remount /system
                    touch /etc/init.d/99cbc
                    chmod 755 /etc/init.d/99cbc
                    mount -o ro,remount /system
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Settings"
                    echo ""
                    echo " File created!"
                    echo ""
                    echo " Press any key to return to the settings menu."
                    echo ""
                    read -n 1
                    others_menu

               elif [ "$MENUINPUT" == "n" ]
               then
                    others_menu
               else
                    clear_output
                    echo ""
                    echo " ERROR!"
                    echo " You have entered an invalid selection!"
                    echo " Press any key to return to the main menu."
                    echo ""
                    read -n 1
                    main_menu
               fi
               if [ "$MENUINPUT" == "b" ]
               then
                    others_menu
               elif [ "$MENUINPUT" == "B" ]
               then
                    others_menu
               else
                    clear_output
                    echo ""
                    echo " ERROR!"
                    echo " You have entered an invalid selection!"
                    echo " Press any key to return to the main menu."
                    echo ""
                    read -n 1
                    main_menu
               fi
          fi
     elif [ "$MENUINPUT" == "2" ]
     then
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Others - Drop caches"
          echo ""
          echo " Free up:"
          echo "  1. Pagecache"
          echo "  2. Dentries and inodes"
          echo "  3. Pagecache, dentries and inodes"
          echo ""
          echo "  b. Return to the edit menu"
          echo ""
          read -n 1 MENUINPUT
          if [ "$MENUINPUT" == "1" ]
          then
               echo "1" > /proc/sys/vm/drop_caches
               others_menu
          elif [ "$MENUINPUT" == "2" ]
          then
               echo "2" > /proc/sys/vm/drop_caches
               others_menu
          elif [ "$MENUINPUT" == "3" ]
          then
               echo "3" > /proc/sys/vm/drop_caches
               others_menu
          fi
          if [ "$MENUINPUT" == "b" ]
          then
               others_menu
          elif [ "$MENUINPUT" == "B" ]
          then
               others_menu
          else
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
          fi
     elif [ "$MENUINPUT" == "b" ]
     then
          main_menu
     elif [ "$MENUINPUT" == "B" ]
     then
          main_menu
     else
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
     fi
}

profiles_menu () {
     echo "To be done..."
}

updates_menu () {
     
     if [ -e "version_number.txt" ]
     then
          NEW_VERSION=$(cat version_number.txt)
          COMPARE=$(awk 'BEGIN {print ("'$NEW_VERSION'" > "'$VERSION'")}')
     else
          NEW_VERSION="unknown"
     fi

     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Updater"
     echo ""
     echo " Updates channel: $CHANNEL"
     echo " Installed version: $VERSION"
     if [ ! "$NEW_VERSION" == "unknown" ]
     then
          echo " Latest version: $NEW_VERSION"
     fi
     echo ""
     echo ""
     echo " Select option:"
     echo "  1. Check for updates"
     echo "  2. Change updates channel"
     echo "  3. View changelog"
     if [ ! "$NEW_VERSION" == "unknown" ]
     then
          if [ $COMPARE == 1 ]
          then
               echo "  4. Install updates"
          fi
     fi
     echo ""
     echo "  b. Back to main menu"
     echo ""
     read -n 1 MENUINPUT
     
     if [ $MENUINPUT == 1 ]
     then
          clear_output
          echo "Checking for updates..."
          echo ""
          if [ "$CHANNEL" == "dev" ]
          then
               curl --progress-bar -L -o version_number.txt "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/version_number/version_number_dev.txt?inline=false"
          elif [ "$CHANNEL" == "stable" ]
          then
               curl --progress-bar -L -o version_number.txt "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/version_number/version_number.txt?inline=false"
          fi
          updates_menu
     elif [ $MENUINPUT == 2 ]
     then
          NEWCHANNEL=$(cat CBC/channel.txt)

          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Updater - Channels"
          echo ""
          echo " Updates channel: $CHANNEL"
          echo ""
          echo " Select option:"
          if [ "$CHANNEL" == "stable" ]
          then
               echo "  1. Stable (in use)" 
               echo "  2. Development"
          elif [ "$CHANNEL" == "dev" ]
               then
               echo "  1. Stable" 
               echo "  2. Development (in use)"
          fi
          
          echo ""
          echo "  b. Back to main menu"
          echo ""
          read -n 1 MENUINPUT
          if [ $MENUINPUT == 1 ]
          then
               if [ "$NEWCHANNEL" == "$CHANNEL" ]
               then
                    clear_output
                    echo ""
                    echo " Channel is already set."
                    echo ""
                    echo " Press any key to return."
                    echo ""
                    read -n 1
                    updates_menu
               else
                    clear_output
                    echo ""
                    echo " STABLE channel set!"
                    echo "stable" > CBC/channel.txt
                    echo ""
                    echo " Automaticlly updating to latest stable release..."
                    echo ""
                    echo " Downloading..."
                    echo ""
                    curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc.sh?inline=false"
                    echo ""
                    echo " Installing and configurating..."
                    chmod 755 cbc
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| "
                    echo ""
                    echo " CBC updated successfully!"
                    echo ""
                    echo " Press any key to restart CBC"
                    read -n 1
                    bash cbc
                    exit 0
               fi
          elif [ $MENUINPUT == 2 ]
          then
               #TODO add warning
               clear_output
               echo ""
               NEWCHANNEL=$(cat CBC/channel.txt)
               if [ "$NEWCHANNEL" == "$CHANNEL" ]
               then
                    clear_output
                    echo ""
                    echo " Channel is already set."
                    echo ""
                    echo " Press any key to return."
                    echo ""
                    read -n 1
                    updates_menu
               else
                    clear_output
                    echo ""
                    echo " DEVELOPMENT channel set!"
                    echo "dev" > CBC/channel.txt
                    echo ""
                    echo " Automaticlly updating to latest development release..."
                    echo ""
                    echo " Downloading..."
                    echo ""
                    curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc-dev.sh?inline=false"
                    echo ""
                    echo " Installing and configurating..."
                    chmod 755 cbc

                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| "
                    echo ""
                    echo " CBC updated successfully!"
                    echo ""
                    echo " Press any key to restart CBC"
                    read -n 1
                    bash cbc
                    exit 0
               fi
          fi
          if [ "$MENUINPUT" == "b" ]
          then
               main_menu
          elif [ "$MENUINPUT" == "B" ]
          then
               main_menu
          else
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
          fi
     elif [ $MENUINPUT == 3 ]
     then
          clear_output
          echo " Downloading..."
          echo ""
          curl --progress-bar -L -o changelog.txt "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/CHANGELOG?inline=false"
          clear_output
          x=0
          for i in $(seq 1 50)
          do  
             echo ""
          done
          echo " ################### CHANGELOG START ###################"
          echo ""
          tac changelog.txt #TODO Make it looks better
          echo "Wisniew : $VERSION"
          echo ""
          echo " Press any key to return."
          echo ""
          read -n 1
          rm -rf changelog.txt
          updates_menu
          
     elif [ $COMPARE == 1 ] #TODO base on commits in dev channel
     then
          if [ $MENUINPUT == 4 ]
          then
               clear_output
               echo " Downloading..."
               echo ""
               if [ "$CHANNEL" == "dev" ]
               then
                    curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc-dev.sh?inline=false"
               elif [ "$CHANNEL" == "stable" ]
               then
                    curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc.sh?inline=false"
               fi
               echo ""
               echo " Installing and configurating..."
               chmod 755 cbc
               #TODO updates notes

               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| "
               echo ""
               echo " CBC updated successfully!"
               echo ""
               echo " Press any key to restart CBC"
               read -n 1
               bash cbc
               exit 0
          fi
     fi
     if [ "$MENUINPUT" == "b" ]
     then
          main_menu
     elif [ "$MENUINPUT" == "B" ]
     then
          main_menu
     else
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
     fi
}

settings_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Settings"
     echo ""
     echo " Select option: "
     echo "  1. Updates"
     echo ""
     echo "  b. Return to the main menu"
     echo ""
     read -n 1 MENUINPUT
     if [ "$MENUINPUT" == "1" ]
     then
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Settings - Updates"
          echo ""
          echo " Select option:"
          if [ "$(cat CBC/autoupdates.txt)" == "0" ]
          then
               echo "  1. Auto updates at start (off)"
               if [ "$(cat CBC/autochecks.txt)" == "0" ]
               then
                    echo "  2. Auto checks at start (off)"
               elif [ "$(cat CBC/autochecks.txt)" == "1" ]
               then
                    echo "  2. Auto checks at start (on)"
               fi
          elif [ "$(cat CBC/autoupdates.txt)" == "1" ]
          then
               echo "  1. Auto updates at start (on)"
          fi

          echo ""
          echo "  b. Return to the settings menu"
          echo ""
          read -n 1 MENUINPUT
          if [ "$MENUINPUT" == "1" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Settings - Updates - Autoupdates"
               echo ""
               if [ "$(cat CBC/autoupdates.txt)" == "0" ]
               then
                    echo " Do You want to enable auto updates at start? (y/n)"
               elif [ "$(cat CBC/autoupdates.txt)" == "1" ]
               then
                    echo " Do You want to disable auto updates at start? (y/n)"
               fi
               echo ""
               read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "y" ]
               then
                    if [ "$(cat CBC/autoupdates.txt)" == "0" ]
                    then
                         echo "1" > CBC/autoupdates.txt
                         settings_menu
                    elif [ "$(cat CBC/autoupdates.txt)" == "1" ]
                    then
                         echo "0" > CBC/autoupdates.txt
                         settings_menu
                    fi
               elif [ "$MENUINPUT" == "n" ]
               then
                    settings_menu
               else
                    settings_menu #TODO warning
               fi
          fi
          if [ "$(cat CBC/autoupdates.txt)" == "0" ]
          then
               if [ "$MENUINPUT" == "2" ]
               then
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Settings - Updates - Autochecks"
                    echo ""
                    if [ "$(cat CBC/autochecks.txt)" == "0" ]
                    then
                         echo " Do You want to enable auto checks for updates at start? (y/n)"
                    elif [ "$(cat CBC/autochecks.txt)" == "1" ]
                    then
                         echo " Do You want to disable auto checks for updates at start? (y/n)"
                    fi
                    echo ""
                    read -n 1 MENUINPUT
                    if [ "$MENUINPUT" == "y" ]
                    then
                         if [ "$(cat CBC/autochecks.txt)" == "0" ]
                         then
                              echo "1" > CBC/autochecks.txt
                              settings_menu
                         elif [ "$(cat CBC/autochecks.txt)" == "1" ]
                         then
                              echo "0" > CBC/autochecks.txt
                              settings_menu
                         fi
                    elif [ "$MENUINPUT" == "n" ]
                    then
                         settings_menu
                    else
                         settings_menu #TODO warning
                    fi
               fi
          fi
          if [ "$MENUINPUT" == "b" ]
          then
               settings_menu
          elif [ "$MENUINPUT" == "B" ]
          then
               settings_menu
          else
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
          fi
     fi
     if [ "$MENUINPUT" == "b" ]
     then
          main_menu
     elif [ "$MENUINPUT" == "B" ]
     then
          main_menu
     else
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
     fi
}

about_menu () {

     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| About"
     echo ""
     echo " Cherry Bash commander is licenced under GNU GPL v3."
     echo " Tool is created by Wisniew."
     echo " CBC script helps configure and control Cherry kernel."
     echo " Please configure Cherry kernel with this tool only."
     echo " Issues/features requests are only checked at gitlab."
     echo " Thanks for using Cherry projects!"
     echo ""
     echo ""
     echo " Press any key to return."
     echo ""
     read -n 1
     main_menu
}

#
# Start
#

if [ ! "$EUID" -ne 0 ]
then
     if [ ! "$1" == "--ignore-autoupdates" ]
     then
          #
          # Prepering
          #

          rm -rf version_number.txt

          if [ ! -e "CBC" ] # mkdir -p CBC?
          then
               mkdir CBC
          fi

          if [ ! -e "CBC/channel.txt" ]
          then
               touch CBC/channel.txt
          fi

          echo "$CHANNEL" > CBC/channel.txt

          if [ ! -e "CBC/autochecks.txt" ]
          then
               touch CBC/autochecks.txt
               echo "0" > CBC/autochecks.txt
          fi

          if [ ! -e "CBC/autoupdates.txt" ]
          then
               touch CBC/autoupdates.txt
               echo "0" > CBC/autoupdates.txt
          fi

          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| "
          echo ""
          echo "Starting..."
          echo ""
          if [ "$(cat CBC/autoupdates.txt)" == "1" ]
          then
               if [ "$CHANNEL" == "stable" ]
               then
                    echo "Checking for updates..."
                    echo ""
                    curl --progress-bar -L -o version_number.txt "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/version_number/version_number.txt?inline=false"
                    NEW_VERSION=$(cat version_number.txt)
                    COMPARE=$(awk 'BEGIN {print ("'$NEW_VERSION'" > "'$VERSION'")}')
                    if [ "$COMPARE" == "1" ]
                    then
                         echo ""
                         echo "Updating..."
                         echo ""
                         curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc.sh?inline=false"
                         chmod 755 cbc

                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| "
                         echo ""
                         echo " CBC updated successfully!"
                         echo ""
                         echo " Press any key to restart CBC"
                         read -n 1
                         bash cbc
                         exit 0
                    fi
               elif [ "$CHANNEL" == "dev" ]
               then
                    echo ""
                    echo "Updating..."
                    echo ""
                    curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc-dev.sh?inline=false"
                    chmod 755 cbc
                    bash cbc --ignore-autoupdates
                    exit 0
               fi
          else
               if [ "$(cat CBC/autochecks.txt)" == "1" ]
               then
                    echo "Checking for updates..."
                    echo ""
                    if [ "$CHANNEL" == "dev" ]
                    then
                         curl --progress-bar -L -o version_number.txt "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/version_number/version_number_dev.txt?inline=false"
                    elif [ "$CHANNEL" == "stable" ]
                    then
                         curl --progress-bar -L -o version_number.txt "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/version_number/version_number.txt?inline=false"
                    fi
               fi
          fi
     fi
     main_menu
else
     echo ""
     echo "You need ROOT premissions to run CBC!"
     echo ""
fi