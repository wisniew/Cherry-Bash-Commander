#!/system/bin/bash

#    Copyright 2018, 2019, 2022 Rafal Wisniewski (wisniew99@gmail.com)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


#########################
# Cherry Bash Commander #
#          0.4          #
#########################

#
# Variables
#

VERSION="0.403"
CHANNEL="dev"

#
# Other functions
#

clear_output () {
     echo -e '\0033\0143'
}

cat2 () {
     if [ "-n" == "$1" ]
      then
          filename=$2
          while read line; do
               echo -n $line
          done < $filename
     else
          filename=$1
          while read line; do
               echo $line
          done < $filename
     fi
}

#
# Start
#

#TODO Auto update process toggable in options
#TODO Prepering screen with chacking for updates

#
# env variables
#
if [ -e ~/.jolla-startupwizard-done ]
then
     OS="salifish"
else
     OS="android"
fi

case $1 in
     "-h" | "--help")

          echo ""
          echo "Cherry Bash Commander licenced under GNU GPL v3"
          echo "by Wisniew"
          echo ""
          echo ""
          echo "To normally run CBC execute:"
          echo "cbc"
          echo ""
          echo "Emergency variables:"
          echo ""
          echo "-fu --force-update     Force update script to latest stable version"
          echo "-fu-dev --force-update-dev     Force update script to latest dev version"
          echo ""
          exit 0

          ;;

     "-fu" | "--force-update")
          
          echo ""
          echo " Downloading..."
          echo ""
          curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc.sh?inline=false"
          echo ""
          echo " Installing and configurating..."
          chmod 755 cbc
          echo ""
          echo " Done!"
          echo ""
          exit 0

          ;;

     "-fu-dev" | "--force-update-dev")

          echo ""
          echo " Downloading..."
          echo ""
          curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc-dev.sh?inline=false"
          echo ""
          echo " Installing and configurating..."
          chmod 755 cbc
          echo ""
          echo " Done!"
          echo ""
          exit 0

          ;;
esac
# env to restore from backup, clear init.d, reset all settings etc...

#
# Menu functions
#

main_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| "
     echo ""
     if [ "$CHANNEL" = "dev" ]
     then
          echo " Cherry Bash Commander DEV"
     elif [ "$CHANNEL" = "stable" ]
     then
          echo " Cherry Bash Commander $VERSION"
     fi
     echo ""
     echo ""
     echo " Select option:"
     echo "  1. Info"
     echo "  2. Monitor"
     echo "  3. Edit"
     echo "  4. Others"
     echo "  5. Profiles (WIP)"
     echo "  6. Updates"
     echo "  7. Settings"
     echo "  8. About"
     echo ""
     echo "  0. Exit"
     echo ""
     read  -n 1 MENUINPUT

     case $MENUINPUT in
     1)
          info_menu
          ;;
     2)
          monitor_menu
          ;;
     3)
          edit_menu
          ;;
     4)
          others_menu
          ;;
     5)
          profiles_menu
          ;;
     6)
          updates_menu
          ;;
     7)
          settings_menu
          ;;
     8)
          about_menu
          ;;
     0)
          clear_output
          exit 0
          ;;
     *)
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu"
          echo ""
          read -n 1
          main_menu
          ;;
     esac
}

info_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Informer"
     echo ""
     echo " Select option:"
     echo "  1. Software"
     echo "  2. Hardware"
     echo ""
     echo "  3. Show getprop"
     echo ""
     echo "  b. Return to the main menu"
     echo ""
     read -n 1 MENUINPUT

     case $MENUINPUT in
     1)
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Informer - software"
          echo ""
          echo -n " Kernel version: " && uname -r
          echo -n " Hostname: " && hostname
          if [ ! -z $(getprop ro.build.type) ]
          then
               echo -n " Build type: " && getprop ro.build.type
          fi
          if [ ! -z $(getprop ro.build.expect.firmware) ]
          then
               echo -n " Firmware: " && getprop ro.build.expect.firmware
          fi          
          if [ ! -z $(getprop ro.mod.version) ]
          then
               echo -n " ROM Version: " && getprop ro.mod.version
          fi
          if [ ! -z $(getprop ro.keymaster.xxx.release) ]
          then
               echo -n " Base version: " && getprop ro.keymaster.xxx.release
          fi
          if [ ! -z $(getprop ro.build.version.release) ]
          then
               echo -n " ROM version: " && getprop ro.build.version.release
          fi
          if [ ! -z $(getprop ro.build.user) ]
          then
               echo -n " ROM builder: " && getprop ro.build.user
          fi
          if [ ! -z $(getprop ro.mk.maintainer) ]
          then
               echo -n " ROM maintainer: " && getprop ro.mk.maintainer
          fi          
          if [ ! -z $(getprop ro.build.id) ]
          then
               echo -n " Build ID: " && getprop ro.build.id
          fi
          if [ ! -z $(getprop ro.build.host) ]
          then
               echo -n " Build host: " && getprop ro.build.host
          fi
          if [ ! -z $(getprop ro.keymaster.xxx.security_patch) ]
          then
               echo -n " Build secure level: " && getprop ro.build.version.security_patch
          fi
          if [ ! -z $(getprop ro.build.selinux) ]
          then
               if [ "$(getprop ro.build.selinux)" == "0" ]
               then
                    echo -n " Selinux: enforced"
               else
                    echo -n " Selinux: permissive"
               fi
          fi
          echo ""
          echo ""
          echo " Press any key to return."
          echo ""
          read -n 1
          info_menu
          ;;
     2)
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Informer - hardware"
          echo ""
          echo -n " Architecture: " && uname -m
          if [ ! -z $(getprop ro.hardware) ]
          then
               echo -n " Hardware: " && getprop ro.hardware
          fi
          if [ ! -z $(getprop ro.product.device) ]
          then
               echo -n " Device: " && getprop ro.product.device
          fi
          if [ ! -z $(getprop ro.product.vendor.device) ]
          then
               echo -n " Device name: " && getprop ro.product.vendor.device
          fi
          if [ ! -z $(getprop ro.product.manufacturer) ]
          then
               echo -n " Device manufacturer: " && getprop ro.product.manufacturer
          fi
          if [ ! -z $(getprop ro.product.cpu.abi) ]
          then
               echo -n " CPU ABI: " && getprop ro.product.cpu.abi
          fi
          if [ ! -z $(getprop ro.product.board) ]
          then
               echo -n " CPU: " && getprop ro.product.board
          fi
          echo ""
          echo " Press any key to return."
          echo ""
          read -n 1
          info_menu
          ;;
     3)
          clear_output
          for i in $(seq 1 50)
          do  
             echo ""
          done
          echo " ################### GETPROP START ###################"
          echo ""
          getprop #TODO Make it looks better / use less?
          echo ""
          echo " Press any key to return."
          echo ""
          read -n 1
          info_menu
          ;;
     b|B)
          main_menu
          ;;
     *)
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
          ;;
     esac
}

monitor_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Monitor"
     echo ""
     echo " Select option:"
     echo "  1. CPU"
     echo "  2. Temperatures"
     echo "  3. RAM"
     echo ""
     echo "  4. htop"
     echo ""
     echo "  b. Return to the main menu"
     echo ""
     read -n 1 MENUINPUT

     case $MENUINPUT in
     1)
          NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
          BIG=$((NUMBER/2))
          LOOPINTERVAL=$(cat2 CBC/loopinterval)
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Monitor - CPU"
          echo ""
          echo " Select cluster:"
          echo "  1. BIG cluster"
          echo "  2. little cluster"
          echo ""
          echo "  b. Return to the main menu"
          echo ""
          read -n 1 MENUINPUT
          case $MENUINPUT in
          1)
               while :; do
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Monitor - CPU - BIG cluster"
                    echo ""
                    echo " BIG cluster"
                    echo ""
                    echo " Governor:"
                    echo -n "  Current - " && cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_governor
                    echo ""
                    echo " Frequency:"
                    echo -n "  Current - " && cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_cur_freq
                    echo -n "  Max - " && cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_max_freq
                    echo -n "  Min - " && cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_min_freq
                    echo ""
                    echo "Press any key to return."
                    echo ""
                    MENUINPUT=""
                    read -n 1 -t $LOOPINTERVAL MENUINPUT
                    if [ ! -z "$MENUINPUT" ]
                    then
                         monitor_menu
                    fi
               done
               ;;
          2)
               while :; do
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Monitor - CPU - little cluster"
                    echo ""
                    echo " little cluster"
                    echo ""
                    echo " Governor:"
                    echo -n "  Current - " && cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor
                    echo ""
                    echo " Frequency:"
                    echo -n "  Current - " && cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_cur_freq
                    echo -n "  Max - " && cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_max_freq
                    echo -n "  Min - " && cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_min_freq
                    echo ""
                    echo " Press any key to return."
                    echo ""
                    MENUINPUT=""
                    read -n 1 -t $LOOPINTERVAL MENUINPUT
                    if [ ! -z "$MENUINPUT" ]
                    then
                         monitor_menu
                    fi
               done
               ;;
          b|B)
               monitor_menu
               ;;
          *)
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
               ;;
          esac
          ;;
     2)
          LOOPINTERVAL=$(cat2 CBC/loopinterval)
          while :; do
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Monitor - Temperatures"
               echo ""
               echo " Temperatures:"
               number=$(ls -1 /sys/devices/virtual/thermal/ | wc -l)
               number=$((number-31)) #TODO make it universal
               for i in $(seq 1 $number)
               do
                    i=$((i-1))
                    if [ ! "$(cat2 /sys/devices/virtual/thermal/thermal_zone$i/temp)" -le "0" ]
                    then 
                         TEMP=$(cat2 /sys/devices/virtual/thermal/thermal_zone$i/temp)
                         echo -n "  " && cat2 -n /sys/devices/virtual/thermal/thermal_zone$i/type && echo -n ": ${TEMP:0:+2}C"
                    fi
                    echo ""
               done
               echo ""
               echo " Press any key to return."
               echo ""
               MENUINPUT=""
               read -n 1 -t $LOOPINTERVAL MENUINPUT
               if [ ! -z "$MENUINPUT" ]
               then
                    monitor_menu
               fi
          done
          ;;
     3)
          LOOPINTERVAL=$(cat2 CBC/loopinterval)
          while :; do
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Monitor - RAM"
               echo ""
               echo " Memory usage:"
               echo -n "  " && cat2 /proc/meminfo | grep "MemTotal"
               echo -n "  " && cat2 /proc/meminfo | grep "MemFree"
               echo -n "  " && cat2 /proc/meminfo | grep "MemAvailable"
               echo ""
               echo -n "  " && cat2 /proc/meminfo | grep "Buffers"
               echo -n "  " && cat2 /proc/meminfo | grep "Cached" | grep -v "Swap"
               echo ""
               echo " Press any key to return."
               echo ""
               MENUINPUT=""
               read -n 1 -t $LOOPINTERVAL MENUINPUT
               if [ ! -z "$MENUINPUT" ]
               then
                    monitor_menu
               fi
          done
          ;;
     4)
          htop
          main_menu
          ;;
     b|B)
          main_menu
          ;;
     *)
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
          ;;
     esac
}

edit_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Editor"
     echo ""
     echo " Select section to edit:"
     echo "  1. CPU"
     echo "  2. GPU"
     echo "  3. Memory"
     echo "  4. Audio"
     echo "  5. Other (WIP)"
     echo ""
     echo "  b. Return to the main menu"
     echo ""
     read -n 1 MENUINPUT

     case $MENUINPUT in
     1)
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Editor - CPU"
          echo ""
          echo " Select cluster:"
          echo "  1. BIG cluster"
          echo "  2. little cluster"
          echo ""
          echo "  b. Return to the edit menu"
          read -n 1 MENUINPUT
          case $MENUINPUT in
          1)
               NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
               BIG=$((NUMBER/2))     
               activegovernor=$(cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_governor)

               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - CPU - BIG cluster"
               echo ""
               echo " Select option:"
               echo "  1. Governor"
               echo "  2. Governor tunables"
               echo "  3. Max frequency"
               echo "  4. Min frequency"
               echo ""
               echo "  b. Return to the edit menu"
               read -n 1 MENUINPUT
               case $MENUINPUT in
               1)
                    governors=($(cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_available_governors))

                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - BIG - Governor"
                    echo ""
                    echo " Governors:"
                    for index in ${!governors[*]}
                    do
                         if [ "${governors[$index]}" == "$activegovernor" ]
                         then
                              echo "  $index. ${governors[$index]} (active)"
                         else
                              echo "  $index. ${governors[$index]}"
                         fi
                    done
                    echo ""
                    echo " Change governor to (b to return):"
                    read -n 1 MENUINPUT
                    if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                    then
                         edit_menu
                    else
                         echo "${governors[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_governor
                         edit_menu
                    fi
                    ;;
               2)
                    tunables=($(ls -1 /sys/bus/cpu/devices/cpu$BIG/cpufreq/$activegovernor/))

                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - BIG - Gov tun..."
                    echo ""
                    echo " Tunables:"
                    for index in ${!tunables[*]}
                    do
                         current=$(cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/$activegovernor/${tunables[$index]})
                         echo "  $index. ${tunables[$index]} ($current)" 
                    done
                    echo ""
                    echo " Select tunable (b to return):"
                    read -n 2 MENUINPUT
                    if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                    then
                         edit_menu
                    else
                         current=$(cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/$activegovernor/${tunables[$MENUINPUT]})
                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| Editor - CPU - BIG - Gov tun..."
                         echo ""
                         echo " Change ${tunables[$MENUINPUT]} value from $current to (b to return):"
                         read -n 3 MENUINPUT1
                         if [ "$MENUINPUT1" == "b" ] || [ "$MENUINPUT1" == "B" ]
                         then
                              edit_menu
                         else
                              echo "$MENUINPUT1" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/$activegovernor/${tunables[$MENUINPUT]}
                              edit_menu
                         fi
                    fi
                    ;;
               3)
                    NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
                    BIG=$((NUMBER/2))
                    freqs=($(cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_available_frequencies))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - BIG - Max freq"
                    echo ""
                    echo -n " Current frequency: " && cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_max_freq
                    echo ""
                    echo " Frequencies:"
                    for ((i=0; i< "${#freqs[@]}"; i++))
                    do
                         echo -n "  $i. ${freqs[$i]}"
                         i=$((i+1))

                         if (( $i > 10 ))
                         then
                              echo -n "   "
                         else
                              echo -n "    "
                         fi

                         if [ ! "${freqs[$i]}" = "" ]
                         then
                              echo "$i. ${freqs[$i]}"
                         else
                              echo ""
                         fi
                    done
                    echo ""
                    echo " Change frequency to (b to return):"
                    read -n 2 MENUINPUT
                    if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                    then
                         edit_menu
                    else
                         echo "${freqs[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_max_freq
                         edit_menu
                    fi
                    ;;
               4)
                    NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
                    BIG=$((NUMBER/2))
                    freqs=($(cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_available_frequencies))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - BIG - Min freq"
                    echo ""
                    echo -n " Current frequency: " && cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_min_freq
                    echo ""
                    echo " Frequencies:"
                    for ((i=0; i< "${#freqs[@]}"; i++))
                    do
                         echo -n "  $i. ${freqs[$i]}"
                         i=$((i+1))

                         if (( $i > 10 ))
                         then
                              echo -n "   "
                         else
                              echo -n "    "
                         fi

                         if [ ! "${freqs[$i]}" = "" ]
                         then
                              echo "$i. ${freqs[$i]}"
                         else
                              echo ""
                         fi
                    done
                    echo ""
                    echo " Change frequency to (b to return):"
                    read -n 2 MENUINPUT
                    if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                    then
                         edit_menu
                    else
                         echo "${freqs[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_min_freq
                         edit_menu
                    fi
               esac
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    clear_output
                    echo ""
                    echo " ERROR!"
                    echo " You have entered an invalid selection!"
                    echo " Press any key to return to the main menu."
                    echo ""
                    read -n 1
                    main_menu
               fi
               ;;
          2)
               activegovernor=$(cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor)

               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - CPU - little cluster"
               echo ""
               echo " Select option:"
               echo "  1. Governor"
               echo "  2. Governor tunables"
               echo "  3. Max frequency"
               echo "  4. Min frequency"
               echo ""
               echo "  b. Return to the edit menu"
               read -n 1 MENUINPUT
               case $MENUINPUT in
               1)
                    governors=($(cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_available_governors))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - little - Governor"
                    echo ""
                    echo " Governors:"
                    for index in ${!governors[*]}
                    do
                         if [ "${governors[$index]}" == "$activegovernor" ]
                         then
                              echo "  $index. ${governors[$index]} (active)"
                         else
                              echo "  $index. ${governors[$index]}"
                         fi
                    done
                    echo ""
                    echo " Change governor to (b to return):"
                    read -n 1 MENUINPUT
                    if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                    then
                         edit_menu
                    else
                         echo "${governors[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor
                         edit_menu
                    fi
                    ;;
               2)
                    tunables=($(ls -1 /sys/bus/cpu/devices/cpu0/cpufreq/$activegovernor/))

                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - little - Gov tun.."
                    echo ""
                    echo " Tunables:"
                    for index in ${!tunables[*]}
                    do
                         current=$(cat2 /sys/bus/cpu/devices/cpu0/cpufreq/$activegovernor/${tunables[$index]})
                         echo "  $index. ${tunables[$index]} ($current)"
                    done
                    echo ""
                    echo " Select tunable (b to return):"
                    read -n 2 MENUINPUT
                    if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                    then
                         edit_menu
                    else
                         current=$(cat2 /sys/bus/cpu/devices/cpu0/cpufreq/$activegovernor/${tunables[$MENUINPUT]})
                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| Editor - CPU - little - Gov tun.."
                         echo ""
                         echo " Change ${tunables[$MENUINPUT]} value from $current to (b to return):"
                         read -n 3 MENUINPUT1
                         if [ "$MENUINPUT1" == "b" ] || [ "$MENUINPUT1" == "B" ]
                         then
                              edit_menu
                         else
                              echo "$MENUINPUT1" > /sys/bus/cpu/devices/cpu0/cpufreq/$activegovernor/${tunables[$MENUINPUT]}
                              edit_menu
                         fi
                    fi
                    ;;
               3)
                    freqs=($(cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_available_frequencies))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - little - Max freq"
                    echo ""
                    echo -n " Current frequency: " && cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_max_freq
                    echo ""
                    echo " Frequencies:"
                    for ((i=0; i< "${#freqs[@]}"; i++))
                    do
                         echo -n "  $i. ${freqs[$i]}"
                         i=$((i+1))

                         if (( $i > 10 ))
                         then
                              echo -n "   "
                         else
                              echo -n "    "
                         fi

                         if [ ! "${freqs[$i]}" = "" ]
                         then
                              echo "$i. ${freqs[$i]}"
                         else
                              echo ""
                         fi
                    done
                    echo ""
                    echo " Change frequency to (b to return):"
                    read -n 2 MENUINPUT
                    if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                    then
                         edit_menu
                    else
                         echo "${freqs[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_max_freq
                         edit_menu
                    fi
                    ;;
               4)
                    NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
                    BIG=$((NUMBER/2))
                    freqs=($(cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_available_frequencies))
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - CPU - little - Min freq"
                    echo ""
                    echo -n " Current frequency: " && cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_min_freq
                    echo ""
                    echo " Frequencies:"
                    for ((i=0; i< "${#freqs[@]}"; i++))
                    do
                         echo -n "  $i. ${freqs[$i]}"
                         i=$((i+1))

                         if (( $i > 10 ))
                         then
                              echo -n "   "
                         else
                              echo -n "    "
                         fi

                         if [ ! "${freqs[$i]}" = "" ]
                         then
                              echo "$i. ${freqs[$i]}"
                         else
                              echo ""
                         fi
                    done
                    echo ""
                    echo " Change frequency to (b to return):"
                    read -n 2 MENUINPUT
                    if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                    then
                         edit_menu
                    else
                         echo "${freqs[$MENUINPUT]}" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_min_freq
                         edit_menu
                    fi
                    ;;
               b|B)
                    edit_menu
                    ;;
               *)
                    clear_output
                    echo ""
                    echo " ERROR!"
                    echo " You have entered an invalid selection!"
                    echo " Press any key to return to the main menu."
                    echo ""
                    read -n 1
                    main_menu
                    ;;
               esac
               ;;
          b|B)
               edit_menu
               ;;
          *)
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
               ;;
          esac
          ;;
     2)
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Editor - GPU"
          echo ""
          echo " Select section to edit:"
          if [ -e /sys/devices/platform/kcal_ctrl.0/kcal_enable ]
          then
               echo "  1. KCAL"
          fi
          echo ""
          echo "  b. Return to main editor menu"
          echo ""
          read -n 1 MENUINPUT
          if [ -e /sys/devices/platform/kcal_ctrl.0/kcal_enable ]
          then
               case $MENUINPUT in
               1)
                    if [ $(cat2 /sys/devices/platform/kcal_ctrl.0/kcal_enable) = 0 ]
                    then
                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                         echo ""
                         echo " KCAL is disabled!"
                         echo " Do You want to enable? (y/n)"
                         echo ""
                         read -n 1 MENUINPUT
                         case $MENUINPUT in
                         y|Y)
                              echo "1" > /sys/devices/platform/kcal_ctrl.0/kcal_enable
                              ;;
                         n|N)
                              edit_menu
                              ;;
                         esac
                    else
                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                         echo ""
                         echo " Select option to edit: "
                         echo "  1. Disable KCAL"
                         echo -n "  2. Colors (" && cat2 -n /sys/devices/platform/kcal_ctrl.0/kcal && echo ")"
                         echo -n "  3. Contrast ("&& cat2 -n /sys/devices/platform/kcal_ctrl.0/kcal_cont && echo ")"
                         echo -n "  4. Saturation ("&& cat2 -n /sys/devices/platform/kcal_ctrl.0/kcal_sat && echo ")"
                         echo -n "  5. Minimal value ("&& cat2 -n /sys/devices/platform/kcal_ctrl.0/kcal_min && echo ")"
                         echo -n "  6. Hue ("&& cat2 -n /sys/devices/platform/kcal_ctrl.0/kcal_hue && echo ")"
                         echo -n "  7. Value ("&& cat2 -n /sys/devices/platform/kcal_ctrl.0/kcal_val && echo ")"
                         #echo -n " 8. Colors invert ("&& cat2 -n /sys/devices/platform/kcal_ctrl.0/kcal_invert && echo ")"
                         echo ""
                         echo "  b. back to edit menu"
                         echo ""
                         read -n 1 MENUINPUT
                         case $MENUINPUT in
                         1)
                              echo "0" > sys/devices/platform/kcal_ctrl.0/kcal_enable
                              ;;
                         2)
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Colors are set to: " && cat2 /sys/devices/platform/kcal_ctrl.0/kcal #TODO Display only specific color
                              echo " value: 0-255 default: 255"
                              echo " Changing red color"
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                              then
                                   edit_menu
                              else
                                   COLORS="$MENUINPUT"
                                   clear_output
                                   echo "   _____ ____   _____  "
                                   echo "  / ____|  _ \ / ____| "
                                   echo " | |    | |_) | |      "
                                   echo " | |    |  _ <| |      "
                                   echo " | |____| |_) | |____  "
                                   echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                                   echo ""
                                   echo -n " Colors are set to: " && cat2 /sys/devices/platform/kcal_ctrl.0/kcal #TODO Display only specific color
                                   echo " value: 0-255 default: 255"
                                   echo " Changing green color"
                                   echo ""
                                   echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                                   if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                                   then
                                        edit_menu
                                   else
                                        COLORS="$COLORS $MENUINPUT"
                                        clear_output
                                        echo "   _____ ____   _____  "
                                        echo "  / ____|  _ \ / ____| "
                                        echo " | |    | |_) | |      "
                                        echo " | |    |  _ <| |      "
                                        echo " | |____| |_) | |____  "
                                        echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                                        echo ""
                                        echo -n " Colors are set to: " && cat2 /sys/devices/platform/kcal_ctrl.0/kcal #TODO Display only specific color
                                        echo " value: 0-255 default: 255"
                                        echo " Changing blue color"
                                        echo ""
                                        echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                                        if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                                        then
                                             edit_menu
                                        else
                                             COLORS="$COLORS $MENUINPUT"
                                             echo "$COLORS" > /sys/devices/platform/kcal_ctrl.0/kcal
                                             edit_menu
                                        fi
                                        edit_menu
                                   fi
                                   edit_menu
                              fi
                              ;;
                         3) #Contrast
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Contrast is set to: " && cat2 /sys/devices/platform/kcal_ctrl.0/kcal_cont
                              echo " value: 0-610 default: 255" #610 is max???
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                              then
                                   edit_menu
                              else
                                   echo "$MENUINPUT" > /sys/devices/platform/kcal_ctrl.0/kcal_cont
                                   edit_menu
                              fi
                              ;;
                         4) # Saturation
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Saturation is set to: " && cat2 /sys/devices/platform/kcal_ctrl.0/kcal_sat
                              echo " value: 0-610 default: 255" #TODO 610 is max??? TEST IT
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                              then
                                   edit_menu
                              else
                                   echo "$MENUINPUT" > /sys/devices/platform/kcal_ctrl.0/kcal_sat
                                   edit_menu
                              fi
                              ;;
                         5) # Minimal
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Minimal value is set to: " && cat2 /sys/device/platform/kcal_ctrl.0/kcal_min
                              echo " value: 0-255 default: 35"
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                              then
                                   edit_menu
                              else
                                   echo "$MENUINPUT" > /sys/devices/platform/kcal_ctrl.0/kcal_min
                                   edit_menu
                              fi
                              ;;
                         6) # hue
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Hue is set to: " && cat2 /sys/devices/platform/kcal_ctrl.0/kcal_hue
                              echo " value: 0-255 default: 0"
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                              then
                                   edit_menu
                              else
                                   echo "$MENUINPUT" > /sys/device/platform/kcal_ctrl.0/kcal_hue
                                   edit_menu
                              fi
                              ;;
                         7) # value
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Editor - GPU - KCAL"
                              echo ""
                              echo -n " Value is set to: " && cat2 /sys/devices/platform/kcal_ctrl.0/kcal_val
                              echo " value: 0-610 default: 255" #TODO 610 is max??? TEST IT
                              echo ""
                              echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
                              if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                              then
                                   edit_menu
                              else
                                   echo "$MENUINPUT" > /sys/devices/platform/kcal_ctrl.0/kcal_val
                                   edit_menu
                              fi
                              ;;
                         b|B)
                              edit_menu
                              ;;
                         *)
                              clear_output
                              echo ""
                              echo " ERROR!"
                              echo " You have entered an invalid selection!"
                              echo " Press any key to return to the main menu."
                              echo ""
                              read -n 1
                              main_menu
                              ;;
                         esac
                    fi
                    ;;
               esac
          elif [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
          then
               edit_menu
          else
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
          fi
          ;;
     3)

          ioschedulers=($(cat2 /sys/block/sda/queue/scheduler))

          #Schedulers without []
          ioschedulersall=($(echo "${ioschedulers[*]}" | sed 's/[][]//g'))
          #Active scheduler without []
          IOSCHEDULERACTIVE="$(echo "${ioschedulers[*]}" | awk -F'[][]' '{print $2}')"

          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Editor - Memory"
          echo ""
          echo " Select section to edit:"
          echo "  1. I/O scheduler ($IOSCHEDULERACTIVE)"
          echo ""
          echo -n "  2. ReadAHead (" && cat2 -n /sys/block/sda/bdi/read_ahead_kb && echo ")"
          echo -n "  3. Swappiness (" && cat2 -n /proc/sys/vm/swappiness && echo ")"
          echo -n "  4. VFS cache pressure (" && cat2 -n /proc/sys/vm/vfs_cache_pressure && echo ")"
          echo -n "  5. Dirty ratio (" && cat2 -n /proc/sys/vm/dirty_ratio && echo ")"
          echo -n "  6. Dirty background ratio (" && cat2 -n /proc/sys/vm/dirty_background_ratio && echo ")"
          echo -n "  7. Extra free kbytes (" && cat2 -n /proc/sys/vm/extra_free_kbytes && echo ")"
          echo -n "  8. Laptop mode (" && cat2 -n /proc/sys/vm/laptop_mode && echo ")"
          echo -n "  9. I/O statistics (" && cat2 -n /sys/block/sda/queue/iostats && echo ")"
          echo ""
          echo "  b. Return to main editor menu"
          echo ""
          read -n 1 MENUINPUT
          case $MENUINPUT in
          1)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - I/O scheduler"
               echo ""
               echo " Current I/O scheduler: $IOSCHEDULERACTIVE"
               echo ""
               echo " Select I/O scheduler:"
               for index in ${!ioschedulersall[*]}
               do
                   printf "  %4d. %s\n" $index ${ioschedulersall[$index]}
               done
               echo ""
               echo " Change I/O scheduler to (b to return):"
               read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "${ioschedulersall[$MENUINPUT]}" > /sys/block/sda/queue/scheduler
                    edit_menu
               fi
               ;;
          2)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - ReadAHead"
               echo ""
               echo -n " ReadAHead is set to: " && cat2 /sys/block/sda/bdi/read_ahead_kb
               echo " value: 0-8096 default: 128"
               echo ""
               echo -n " Change value to (or b to return):" && read -n 4 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /sys/block/sda/bdi/read_ahead_kb
                    edit_menu
               fi
               ;;
          3)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - Swappiness"
               echo ""
               echo -n " Swappiness is set to: " && cat2 /proc/sys/vm/swappiness
               echo " value: 0-200 default: 60"
               echo ""
               echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/swappiness
                    edit_menu
               fi
               ;;
          4)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - VFS cache pr..."
               echo ""
               echo -n " VFS cache pressure is set to: " && cat2 /proc/sys/vm/vfs_cache_pressure
               echo " value: 0-9999 default: 100"
               echo ""
               echo -n " Change value to (or b to return):" && read -n 4 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/vfs_cache_pressure
                    edit_menu
               fi
               ;;
          5)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - Dirty ratio"
               echo ""
               echo -n " Dirty ratio is set to: " && cat2 /proc/sys/vm/dirty_ratio
               echo " value: 0-9999 default: 20" #TODO to test!
               echo ""
               echo -n " Change value to (or b to return):" && read -n 4 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/dirty_ratio
                    edit_menu
               fi
               ;;
          6)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - Dirty backgr..."
               echo ""
               echo -n " Dirty background ratio is set to: " && cat2 /proc/sys/vm/dirty_background_ratio
               echo " value: 0-9999 default: 5" #TODO to test!
               echo ""
               echo -n " Change value to (or b to return):" && read -n 4 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/dirty_background_ratio
                    edit_menu
               fi
               ;;
          7)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - Extra free k..."
               echo ""
               echo -n " Extra free kbytes is set to: " && cat2 /proc/sys/vm/extra_free_kbytes
               echo " value: 0-999999" #TODO to test!
               echo ""
               echo -n " Change value to (or b to return):" && read -n 6 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/extra_free_kbytes
                    edit_menu
               fi
               ;;
          8)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - Laptop mode"
               echo ""
               echo -n " Laptop mode is set to: " && cat2 /proc/sys/vm/laptop_mode
               echo " value: 0-1 default: 0" #TODO change to on/off
               echo ""
               echo -n " Change value to (or b to return):" && read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /proc/sys/vm/laptop_mode
                    edit_menu
               fi
               ;;
          9)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Memory - I/O stats"
               echo ""
               echo -n " I/O statistics is set to: " && cat2 /sys/block/sda/queue/iostats
               echo " value: 0-1 default: 1" #TODO change to on/off
               echo ""
               echo -n " Change value to (or b to return):" && read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /sys/block/sda/queue/iostats
                    edit_menu
               fi
               ;;
          b|B)
               edit_menu
               ;;
          *)
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
               ;;
          esac
          ;;
     4)
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Editor - Audio"
          echo ""
          echo " Select section to edit:"
          echo -n "  1. Earpiece gain (" && cat2 -n /sys/kernel/sound_control/earpiece_gain && echo ")" #TODO convert <256 to -x
          echo -n "  2. Headphone gain (" && cat2 -n /sys/kernel/sound_control/headphone_gain && echo ")" #TODO convert <256 to -x
          echo -n "  3. Mic gain (" && cat2 -n /sys/kernel/sound_control/mic_gain && echo ")" #TODO convert <256 to -x
          echo ""
          echo "  b. Return to main editor menu"
          echo ""
          read -n 1 MENUINPUT
          case $MENUINPUT in
          1)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Audio - Earpiece gain"
               echo ""
               echo -n " Earpiece gain is set to: " && cat2 /sys/kernel/sound_control/earpiece_gain
               echo " value: -10-20 default: 0"
               echo ""
               echo " Change value to (b to return):" && read -n 3 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /sys/kernel/sound_control/earpiece_gain
                    edit_menu
               fi
               ;;
          2)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Audio - Headphone gain"
               echo ""
               echo -n " Left headphone gain is set to: " && cat2 /sys/kernel/sound_control/headphone_gain #TODO display correct channel & convert...
               echo " value: -40-20 default: 0"
               echo ""
               echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Editor - Audio - Headphone gain"
                    echo ""
                    echo -n " Right headphone gain is set to: " && cat2 /sys/kernel/sound_control/headphone_gain #TODO display correct channel & convert...
                    echo " value: -40-20 default: 0"
                    echo ""
                    echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT1
                    if [ "$MENUINPUT1" == "b" ]
                    then
                         edit_menu
                    else
                         echo "$MENUINPUT $MENUINPUT1" > /sys/kernel/sound_control/headphone_gain
                         edit_menu
                    fi
                    edit_menu
               fi
               ;;
          3)
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Editor - Audio - Mic gain"
               echo ""
               echo -n " ReadAHead is set to: " && cat2 /sys/kernel/sound_control/mic_gain
               echo " value: -10-20 default: 12"
               echo ""
               echo -n " Change value to (or b to return):" && read -n 3 MENUINPUT
               if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
               then
                    edit_menu
               else
                    echo "$MENUINPUT" > /sys/kernel/sound_control/mic_gain
                    edit_menu
               fi
               ;;
          b|B)
               edit_menu
               ;;
          *)
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
               ;;
          esac
          ;;
     b|B)
          main_menu
          ;;
     *)
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
          ;;
     esac
}

others_menu () {
     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Others"
     echo ""
     echo " Select option:"
     echo "  1. init.d"
     echo "  2. Drop caches"
     echo "  3. Flasher"
     echo "  4. Downloader"
     echo ""
     echo "  b. Return to the main menu"
     echo ""
     read -n 1 MENUINPUT
     case $MENUINPUT in
     1)
          if [ -e /etc/init.d/99cbc ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Others - init.d"
               echo ""
               if [ "$(cat2 /etc/init.d/99cbc)" == "" ]
               then
                    echo " CBC init.d is empty"
               else
                    echo " CBC init.d is present"
               fi
               echo ""
               echo " Select option:"
               echo "  1. View init.d file"
               echo "  2. Push current settings"
               echo "  3. Push settings from profile (WIP)"
               echo "  4. Delete file"
               echo ""
               echo "  b. Return to the settings menu"
               echo ""
               read -n 1 MENUINPUT
               case $MENUINPUT in
               1)
                    for i in $(seq 1 50)
                    do  
                       echo ""
                    done
                    echo " ##################### INIT.D FILE #####################"
                    echo ""
                    cat2 /etc/init.d/99cbc #TODO Make it looks better
                    echo ""
                    echo " Press any key to return."
                    echo ""
                    read -n 1
                    others_menu
                    ;;
               2)
                    NUMBER=$(ls -1 /sys/bus/cpu/devices/ | wc -l)
                    BIG=$((NUMBER/2))

                    mount -o rw,remount /system
                    rm -rf /etc/init.d/99cbc
                    touch /etc/init.d/99cbc
                    chmod 755 /etc/init.d/99cbc


                    echo "#!/system/bin/sh" >> /etc/init.d/99cbc
                    echo "#CBC init.d / profile file" >> /etc/init.d/99cbc
                    echo "#Script is autogenerated by CBC $VERSION" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc

                    echo "#CPU:" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "#BIG cluster" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_governor)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_governor" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_max_freq)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_max_freq" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_min_freq)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu$BIG/cpufreq/scaling_min_freq" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "#little cluster" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_max_freq)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_max_freq" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/bus/cpu/devices/cpu0/cpufreq/scaling_min_freq)
                    echo "echo \"$CURRENT\" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_min_freq" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc

                    echo "#GPU" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    if [ -e /sys/devices/platform/kcal_ctrl.0/kcal_enable ]
                    then
                         echo "#KCAL:" >> /etc/init.d/99cbc
                         CURRENT=$(cat2 /sys/devices/platform/kcal_ctrl.0/kcal_enable)
                         echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_enable" >> /etc/init.d/99cbc
                         CURRENT=$(cat2 /sys/devices/platform/kcal_ctrl.0/kcal_cont)
                         echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_cont" >> /etc/init.d/99cbc
                         CURRENT=$(cat2 /sys/devices/platform/kcal_ctrl.0/kcal_sat)
                         echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_sat" >> /etc/init.d/99cbc
                         CURRENT=$(cat2 /sys/devices/platform/kcal_ctrl.0/kcal_min)
                         echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_min" >> /etc/init.d/99cbc
                         CURRENT=$(cat2 /sys/devices/platform/kcal_ctrl.0/kcal_hue)
                         echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_hue" >> /etc/init.d/99cbc
                         CURRENT=$(cat2 /sys/devices/platform/kcal_ctrl.0/kcal_val)
                         echo "echo \"$CURRENT\" > /sys/devices/platform/kcal_ctrl.0/kcal_val" >> /etc/init.d/99cbc
                    fi
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc

                    echo "#Memory:" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/block/sda/queue/scheduler)
                    echo "echo \"$CURRENT\" > /sys/block/sda/queue/scheduler" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/block/sda/bdi/read_ahead_kb)
                    echo "echo \"$CURRENT\" > /sys/block/sda/bdi/read_ahead_kb" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /proc/sys/vm/swappiness)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/swappiness" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /proc/sys/vm/vfs_cache_pressure)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/vfs_cache_pressure" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /proc/sys/vm/dirty_ratio)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/dirty_ratio" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /proc/sys/vm/dirty_background_ratio)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/dirty_background_ratio" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /proc/sys/vm/extra_free_kbytes)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/extra_free_kbytes" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /proc/sys/vm/laptop_mode)
                    echo "echo \"$CURRENT\" > /proc/sys/vm/laptop_mode" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/block/sda/queue/iostats)
                    echo "echo \"$CURRENT\" > /sys/block/sda/queue/iostats" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc

                    echo "#Audio:" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc

                    echo "#Others:" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/kernel/sound_control/earpiece_gain)
                    echo "echo \"$CURRENT\" > /sys/kernel/sound_control/earpiece_gain" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/kernel/sound_control/headphone_gain)
                    echo "echo \"$CURRENT\" > /sys/kernel/sound_control/headphone_gain" >> /etc/init.d/99cbc
                    CURRENT=$(cat2 /sys/kernel/sound_control/mic_gain)
                    echo "echo \"$CURRENT\" > /sys/kernel/sound_control/mic_gain" >> /etc/init.d/99cbc
                    echo "" >> /etc/init.d/99cbc


                    mount -o ro,remount /system
                    others_menu
                    ;;
               3)
                    others_menu
                    ;;
               4)
                    mount -o rw,remount /system
                    rm -rf /etc/init.d/99cbc
                    mount -o ro,remount /system
                    others_menu
                    ;;
               b|B)
                    others_menu
                    ;;
               *)
                    clear_output
                    echo ""
                    echo " ERROR!"
                    echo " You have entered an invalid selection!"
                    echo " Press any key to return to the main menu."
                    echo ""
                    read -n 1
                    main_menu
                    ;;
               esac
          else
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Others - init.d"
               echo ""
               echo " CBC init.d file not exist!"
               echo ""
               echo " Do You want to create that file (y/n)?"
               echo ""
               read -n 1 MENUINPUT
               case $MENUINPUT in
               y|Y)
                    mount -o rw,remount /system
                    touch /etc/init.d/99cbc
                    chmod 755 /etc/init.d/99cbc
                    mount -o ro,remount /system
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Others - init.d"
                    echo ""
                    echo " File created!"
                    echo ""
                    echo " Press any key to return to the settings menu."
                    echo ""
                    read -n 1
                    others_menu
                    ;;
               n|N|b|B)
                    others_menu
                    ;;
               *)
                    clear_output
                    echo ""
                    echo " ERROR!"
                    echo " You have entered an invalid selection!"
                    echo " Press any key to return to the main menu."
                    echo ""
                    read -n 1
                    main_menu
                    ;;
               esac
          fi
          ;;
     2)
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Others - Drop caches"
          echo ""
          echo " Free up:"
          echo "  1. Pagecache"
          echo "  2. Dentries and inodes"
          echo "  3. Pagecache, dentries and inodes"
          echo ""
          echo "  b. Return to the edit menu"
          echo ""
          read -n 1 MENUINPUT
          case $MENUINPUT in
          1)
          	clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Others - Drop caches"
               echo ""
               echo " Cleaning..."
               echo ""
               echo "1" > /proc/sys/vm/drop_caches
               others_menu
               ;;
          2)
          	clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Others - Drop caches"
               echo ""
               echo " Cleaning..."
               echo ""
               echo "2" > /proc/sys/vm/drop_caches
               others_menu
               ;;
          3)
          	clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Others - Drop caches"
               echo ""
               echo " Cleaning..."
               echo ""
               echo "3" > /proc/sys/vm/drop_caches
               others_menu
               ;;
          b|B)
               others_menu
               ;;
          *)
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
               ;;
          esac
          ;;
     3)
          flasher () {
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Others - Flasher"
               echo ""
               echo " From Where You want to flash?"
               echo "  1. Main phone folder"
               echo "  2. Downloads folder"
               echo "  3. Custom folder"
               echo ""
               echo "  b. Return to te others menu"
               read -n 1 MENUINPUT
               case $MENUINPUT in
               1)
                    LOCATION="/sdcard/"
                    ;;
               2)
                    LOCATION="/sdcard/Download/"
                    ;;
               3)
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Others - Flasher"
                    echo ""
                    echo -n " Type path (starting from main phone folder):" && read MENUINPUT
                    LOCATION="/sdcard/$MENUINPUT/"
                    if [ ! -e "$LOCATION" ]
                    then
                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| Others - Flasher"
                         echo ""
                         echo " Path does not exist!"
                         echo ""
                         echo " Press any key to return."
                         read -n 1
                         others_menu
                    fi
                    ;;
               esac
               case $MENUINPUT in
               1|2|3)
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Others - Flasher"
                    echo ""
                    echo " File flash: "
                    echo ""

                    i=0
                    while read line
                    do
                        files[ $i ]="$line"        
                        (( i++ ))
                    done < <(ls $LOCATION | egrep '\.zip$')

                    for index in ${!files[*]}
                    do
                        echo " $index. ${files[$index]}"
                    done

                    echo ""
                    echo " b. Return to the others menu"
                    echo ""
                    read MENUINPUT
                    if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
                    then
                         others_menu
                    else
                         FILENAME=("${files[$MENUINPUT]}")
                    fi

                    BOOTRECOVERY=$(cat2 /cache/recovery/command | grep "boot-recovery")

                    if [ ! -e "/cache/recovery/command" ]
                    then
                         echo "boot-recovery " > /cache/recovery/command
                    fi
                    echo "--update_package=$LOCATION$FILENAME" >> /cache/recovery/command

                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Others - Flasher - Queue"
                    echo ""
                    echo " Queue:"

                    j=$(cat2 /cache/recovery/command | grep ".zip" | wc -l)
                    for i in $(seq 1 $j)
                    do  
                         echo -n "  $i. "
                         cat2 /cache/recovery/command | grep ".zip" | sed -e 's/--update_package=\(.*\).zip/\1/' | head -$i | tail -1
                    done
                    
                    echo ""
                    echo "  1. add zip to queue"
                    echo "  2. clear queue"
                    echo "  3. flash queue"
                    echo ""
                    echo "  b. back"
                    read -n 1 MENUINPUT
                    case $MENUINPUT in
                    1)
                         flasher
                         ;;
                    2)
                         rm /cache/recovery/command
                         flasher
                         ;;
                    3)
                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| Others - Flasher - Queue"
                         echo ""
                         echo " Do You want to reboot now? (y/n)"
                         read -n 1 MENUINPUT
                         if [ "$MENUINPUT" == "y" ] || [ "$MENUINPUT" == "Y" ]
                         then
                              reboot recovery
                              exit 0
                         else
                              rm /cache/recovery/command
                              others_menu
                         fi
                         ;;
                    b|B)
                         rm /cache/recovery/command
                         others_menu
                         ;;
                    *)
                         rm /cache/recovery/command
                         clear_output
                         echo ""
                         echo " ERROR!"
                         echo " You have entered an invalid selection!"
                         echo " Press any key to return to the main menu."
                         echo ""
                         read -n 1
                         main_menu
                         ;;
                    esac
                    ;;
               b|B)
                    rm /cache/recovery/command
                    others_menu
                    ;;
               *)
                    rm /cache/recovery/command
                    clear_output
                    echo ""
                    echo " ERROR!"
                    echo " You have entered an invalid selection!"
                    echo " Press any key to return to the main menu."
                    echo ""
                    read -n 1
                    main_menu
                    ;;
               esac
          }
          flasher
          ;;
     4)
          PYTHON=$(command -v python)
          PYTHON3=$(command -v python3)
          if [ ! "$PYTHON" == "" ] || [ ! "$PYTHON3" == "" ]
          then
               if [ -e CBC/youtube-dl ]
               then
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Others - Downloader"
                    echo ""
                    echo " Checking youtube-dl version..."
                    if [ ! "$PYTHON" == "" ]
                    then
                         VERSIONYTDL=$(python CBC/youtube-dl --no-warnings --version)
                    else
                         VERSIONYTDL=$(python3 CBC/youtube-dl --no-warnings --version)
                    fi

                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Others - Downloader"
                    echo ""
                    echo -n " Installed version: $VERSIONYTDL"
                    echo ""
                    echo "  1. Download video"
                    echo "  2. Update youtube-dl"
                    echo ""
                    echo "  b. Return to the others menu"
                    read -n 1 MENUINPUT
                    case $MENUINPUT in
                    1)
                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| Others - Downloader"
                         echo ""
                         echo " Where You want to download?"
                         echo ""
                         echo "  1. Main phone folder"
                         echo "  2. Downloads folder"
                         echo "  3. Custom folder"
                         echo ""
                         echo "  b. Return to te others menu"
                         read -n 1 MENUINPUT
                         case $MENUINPUT in
                         1)
                              LOCATION="/sdcard"
                              ;;
                         2)
                              LOCATION="/sdcard/Download"
                              ;;
                         3)
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Others - Downloader"
                              echo ""
                              echo -n " Type path (starting from main phone folder): " && read MENUINPUT
                              LOCATION="/sdcard/$MENUINPUT"
                              if [ ! -e "$LOCATION" ]
                              then
                                   mkdir -p /sdcard/$MENUINPUT
                              fi
                         esac
                         if [ ! "$MENUINPUT" == "b" ] || [ ! "$MENUINPUT" == "B" ]
                         then
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Others - Downloader"
                              echo ""
                              echo " Paste the video URL: (b to return) "
                              read URL
                              if [ "$URL" == "b" ] || [ "$URL" == "B" ]
                              then
                                   others_menu
                              fi

                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Others - Downloader"
                              echo ""
                              echo " Fetching informations..."
                              if [ ! "$PYTHON" == "" ]
                              then
                                   TITLE=$(python CBC/youtube-dl --no-warnings -e $URL)
                                   DURATION=$(python CBC/youtube-dl --no-warnings --get-duration $URL)
                              else
                                   TITLE=$(python3 CBC/youtube-dl --no-warnings -e $URL)
                                   DURATION=$(python3 CBC/youtube-dl --no-warnings --get-duration $URL)
                              fi
                              
                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Others - Downloader"
                              echo ""
                              echo -n " Title: $TITLE"
                              echo -n " Duration: $DURATION"
                              echo ""
                              echo " Download using..."
                              echo ""
                              echo " Default settings:"
                              echo "  1. Video and audio"
                              echo "  2. Audio only"
                              echo ""
                              echo " Custom settings:"
                              echo "  3. Video only / video and audio (WIP)"
                              echo "  4. Audio only (WIP)"
                              echo ""
                              echo " b. Return to the others menu."
                              read -n 1 MENUINPUT
                              case $MENUINPUT in
                              1|2|3|4)
                                   echo ""
                                   ;&
                              b|B)
                                   others_menu
                                   ;;
                              *)
                                   clear_output
                                   echo ""
                                   echo " ERROR!"
                                   echo " You have entered an invalid selection!"
                                   echo " Press any key to return to the main menu."
                                   echo ""
                                   read -n 1
                                   main_menu
                              esac

                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Others - Downloader"
                              echo ""
                              echo " Use video title as filename? (y/n)"
                              read -n 1 MENUINPUT1
                              case $MENUINPUT1 in
                              y|Y)
                                   clear_output
                                   echo "   _____ ____   _____  "
                                   echo "  / ____|  _ \ / ____| "
                                   echo " | |    | |_) | |      "
                                   echo " | |    |  _ <| |      "
                                   echo " | |____| |_) | |____  "
                                   echo "  \_____|____/ \_____| Others - Downloader"
                                   echo ""
                                   echo " Please wait a bit..."
                                   case $MENUINPUT in
                                   1)
                                        if [ ! "$PYTHON" == "" ]
                                        then
                                             FILENAME=$(python CBC/youtube-dl --no-warnings --get-filename $URL)
                                        else
                                             FILENAME=$(python3 CBC/youtube-dl --no-warnings --get-filename $URL)
                                        fi
                                        ;;
                                   2)
                                        if [ ! "$PYTHON" == "" ]
                                        then
                                             FILENAME=$(python CBC/youtube-dl --no-warnings -e $URL)
                                        else
                                             FILENAME=$(python3 CBC/youtube-dl --no-warnings -e $URL)
                                        fi
                                        ;;
                                   esac
                                   ;;
                              n|N)
                                   echo ""
                                   echo " Type new filename (with extensions):"
                                   read FILENAME
                                   ;;
                              *)
                                   clear_output
                                   echo ""
                                   echo " ERROR!"
                                   echo " You have entered an invalid selection!"
                                   echo " Press any key to return to the main menu."
                                   echo ""
                                   read -n 1
                                   main_menu
                                   ;;
                              esac

                              clear_output
                              echo "   _____ ____   _____  "
                              echo "  / ____|  _ \ / ____| "
                              echo " | |    | |_) | |      "
                              echo " | |    |  _ <| |      "
                              echo " | |____| |_) | |____  "
                              echo "  \_____|____/ \_____| Others - Downloader"
                              echo ""

                              case $MENUINPUT in
                              1)
                                   if [ ! "$PYTHON" == "" ]
                                   then
                                        python CBC/youtube-dl --no-warnings -o "$LOCATION/$FILENAME" $URL
                                   else
                                        python3 CBC/youtube-dl --no-warnings -o "$LOCATION/$FILENAME" $URL
                                   fi

                                   if [ -e "$LOCATION/$FILENAME" ]
                                   then
                                        termux-notification --title "CBC - Downloader" --content "$FILENAME downloaded!"
                                   else
                                        termux-notification --title "CBC - Downloader" --content "Something goes wrong!"
                                   fi
                                   
                                   echo ""
                                   echo "Press any key to return to the others menu."
                                   read -n 1
                                   others_menu
                                   ;;
                              2)
                                   if [ ! "$PYTHON" == "" ]
                                   then
                                        python CBC/youtube-dl --no-warnings -x -o "$LOCATION/$FILENAME.mp3" $URL
                                   else
                                        python3 CBC/youtube-dl --no-warnings -x -o "$LOCATION/$FILENAME.mp3" $URL
                                   fi
                                   

                                   if [ -e "$LOCATION/$FILENAME" ]
                                   then
                                        termux-notification --title "CBC - Downloader" --content "$FILENAME downloaded!"
                                   else
                                        termux-notification --title "CBC - Downloader" --content "Something goes wrong!"
                                   fi

                                   echo ""
                                   echo "Press any key to return to the others menu."
                                   read -n 1
                                   others_menu
                                   ;;
                              3)
                                   others_menu
                                   ;;
                              4)
                                   others_menu
                                   ;;
                              esac
                         fi
                         ;;
                    2)
                         clear_output
                         echo ""
                         echo " Removing..."
                         rm -rf /CBC/youtube-dl
                         echo ""
                         echo " Downloading..."
                         echo ""
                         curl --progress-bar -L -o CBC/youtube-dl "https://yt-dl.org/downloads/latest/youtube-dl"
                         chmod 755 CBC/youtube-dl
                         echo ""
                         others_menu
                         ;;
                    b|B)
                         others_menu
                         ;;
                    *)
                         clear_output
                         echo ""
                         echo " ERROR!"
                         echo " You have entered an invalid selection!"
                         echo " Press any key to return to the main menu."
                         echo ""
                         read -n 1
                         main_menu
                         ;;
                    esac
               else
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Others - Downloader"
                    echo ""
                    echo " You don't have youtube-dl installed. "
                    echo ""
                    echo " Download it? (y/n)?"
                    read -n 1 MENUINPUT
                    case $MENUINPUT in
                    y|Y)
                         clear_output
                         echo ""
                         echo " Downloading..."
                         echo ""
                         curl --progress-bar -L -o CBC/youtube-dl "https://yt-dl.org/downloads/latest/youtube-dl"
                         chmod 755 CBC/youtube-dl
                         echo ""
                         others_menu
                         ;;
                    n|N)
                         others_menu
                         ;;
                    *)
                         clear_output
                         echo ""
                         echo " ERROR!"
                         echo " You have entered an invalid selection!"
                         echo " Press any key to return to the main menu."
                         echo ""
                         read -n 1
                         main_menu
                         ;;
                    esac
               fi
          else
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Others - Downloader"
               echo ""
               echo " Cherry Downloader is supported until CBC-installer-6!"
               echo ""
               echo " Install CBC-installer-6 to make it configure everything"
               echo " needed for Cherry Downloader."
               echo ""
               echo " Press any key to return to the others menu."
               echo ""
               read -n 1
               others_menu
          fi
          ;;
     b|B)
          main_menu
          ;;
     *)
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
          ;;
     esac
}

profiles_menu () {
     echo "To be done..."
}

updates_menu () {
     
     if [ -e "version_number" ]
     then
          NEW_VERSION=$(cat2 version_number)
          COMPARE=$(awk 'BEGIN {print ("'$NEW_VERSION'" > "'$VERSION'")}')
     else
          NEW_VERSION="unknown"
     fi

     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Updater"
     echo ""
     echo " Updates channel: $CHANNEL"
     echo " Installed version: $VERSION"
     if [ ! "$NEW_VERSION" == "unknown" ]
     then
          echo " Latest version: $NEW_VERSION"
     fi
     echo ""
     echo ""
     echo " Select option:"
     echo "  1. Check for updates"
     echo "  2. Change updates channel"
     echo "  3. View changelog"
     if [ ! "$NEW_VERSION" == "unknown" ]
     then
          if [ $COMPARE == 1 ]
          then
               echo "  4. Install updates"
          fi
     fi
     echo ""
     echo "  b. Back to main menu"
     echo ""
     read -n 1 MENUINPUT
     
     case $MENUINPUT in
     1)
          clear_output
          echo "Checking for updates..."
          echo ""
          case $CHANNEL in
          dev)
               curl --progress-bar -L -o version_number "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/version_number/version_number_dev.txt?inline=false"
               ;;
          stable)
               curl --progress-bar -L -o version_number "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/version_number/version_number.txt?inline=false"
               ;;
          esac
          updates_menu;;
     2)
          NEWCHANNEL=$(cat2 CBC/channel)

          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Updater - Channels"
          echo ""
          echo " Updates channel: $CHANNEL"
          echo ""
          echo " Select option:"
          case $CHANNEL in
          stable)
               echo "  1. Stable (in use)" 
               echo "  2. Development"
               ;;
          dev)
               echo "  1. Stable" 
               echo "  2. Development (in use)"
               ;;
          esac
          
          echo ""
          echo "  b. Back to main menu"
          echo ""
          read -n 1 MENUINPUT
          case $MENUINPUT in
          1)
               echo "stable" > CBC/channel
               if [ ! "$NEWCHANNEL" == "$CHANNEL" ]
               then
                    clear_output
                    echo ""
                    echo " Channel is already set."
                    echo ""
                    echo " Press any key to return."
                    echo ""
                    read -n 1
                    updates_menu
               else
                    clear_output
                    echo ""
                    echo " STABLE channel set!"
                    echo "stable" > CBC/channel
                    echo ""
                    echo " Automaticlly updating to latest stable release..."
                    echo ""
                    echo " Downloading..."
                    echo ""
                    curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc.sh?inline=false"
                    echo ""
                    echo " Installing and configurating..."
                    chmod 755 cbc
                    
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| "
                    echo ""
                    echo " CBC updated successfully!"
                    echo ""
                    echo " Press any key to restart CBC"
                    read -n 1
                    bash cbc
                    exit 0
               fi
               ;;
          2)
               #TODO add warning
               clear_output
               echo ""
               echo "dev" > CBC/channel
               if [ ! "$NEWCHANNEL" == "$CHANNEL" ]
               then
                    clear_output
                    echo ""
                    echo " Channel is already set."
                    echo ""
                    echo " Press any key to return."
                    echo ""
                    read -n 1
                    updates_menu
               else
                    clear_output
                    echo ""
                    echo " DEVELOPMENT channel set!"
                    echo "dev" > CBC/channel
                    echo ""
                    echo " Automaticlly updating to latest development release..."
                    echo ""
                    echo " Downloading..."
                    echo ""
                    curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc-dev.sh?inline=false"
                    echo ""
                    echo " Installing and configurating..."
                    chmod 755 cbc

                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| "
                    echo ""
                    echo " CBC updated successfully!"
                    echo ""
                    echo " Press any key to restart CBC"
                    read -n 1
                    bash cbc
                    exit 0
               fi
               ;;
          b|B)
               main_menu
               ;;
          *)
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
               ;;
          esac
          ;;
     3)
          clear_output
          echo " Downloading..."
          echo ""
          curl --progress-bar -L -o changelog.txt "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/CHANGELOG?inline=false"
          clear_output
          x=0
          for i in $(seq 1 50)
          do  
             echo ""
          done
          echo " ################### CHANGELOG START ###################"
          echo ""
          tac changelog.txt #TODO Make it looks better
          echo "Wisniew : $VERSION"
          echo ""
          echo " Press any key to return."
          echo ""
          read -n 1
          rm -rf changelog.txt
          updates_menu
          ;;
     esac
     if [ $COMPARE == 1 ] #TODO base on commits in dev channel
     then
          if [ $MENUINPUT == 4 ]
          then
               clear_output
               echo " Downloading..."
               echo ""
               if [ "$CHANNEL" == "dev" ]
               then
                    curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc-dev.sh?inline=false"
               elif [ "$CHANNEL" == "stable" ]
               then
                    curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc.sh?inline=false"
               fi
               echo ""
               echo " Installing and configurating..."
               chmod 755 cbc
               #TODO updates notes

               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| "
               echo ""
               echo " CBC updated successfully!"
               echo ""
               echo " Press any key to restart CBC"
               read -n 1
               bash cbc
               exit 0
          fi
     elif [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
     then
          main_menu
     else
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
     fi
}

settings_menu () {
     LOOPINTERVAL=$(cat2 CBC/loopinterval)

     if [ ! -e .bashrc ]
     then
          touch .bashrc
     fi

     AUTOSTART=$(cat2 .bashrc | grep "cbc")
     if [ "$AUTOSTART" == "cbc" ] 
     then
          AUTOSTART="on"
     else
          AUTOSTART="off"
     fi

     AUTOEXIT=$(cat2 .bashrc | grep "exit")
     if [ "$AUTOEXIT" == "exit" ] 
     then
          AUTOEXIT="on"
     else
          AUTOEXIT="off"
     fi

     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| Settings"
     echo ""
     echo " Select option: "
     echo "  1. Updates"
     echo "  2. Termux"
     echo "  3. Loop interval ($LOOPINTERVAL s)"
     echo "  4. Screen calibration"
     echo ""
     echo "  b. Return to the main menu"
     echo ""
     read -n 1 MENUINPUT
     case $MENUINPUT in
     1)
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Settings - Updates"
          echo ""
          echo " Select option:"
          if [ "$(cat2 CBC/autoupdates)" == "0" ]
          then
               echo "  1. Auto updates at start (off)"
               if [ "$(cat2 CBC/autochecks)" == "0" ]
               then
                    echo "  2. Auto checks at start (off)"
               elif [ "$(cat2 CBC/autochecks)" == "1" ]
               then
                    echo "  2. Auto checks at start (on)"
               fi
          elif [ "$(cat2 CBC/autoupdates)" == "1" ]
          then
               echo "  1. Auto updates at start (on)"
          fi

          echo ""
          echo "  b. Return to the settings menu"
          echo ""
          read -n 1 MENUINPUT
          if [ "$MENUINPUT" == "1" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Settings - Updates - Autoupdates"
               echo ""
               if [ "$(cat2 CBC/autoupdates)" == "0" ]
               then
                    echo " Do You want to enable auto updates at start? (y/n)"
               elif [ "$(cat2 CBC/autoupdates)" == "1" ]
               then
                    echo " Do You want to disable auto updates at start? (y/n)"
               fi
               echo ""
               read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "y" ] || [ "$MENUINPUT" == "Y" ]
               then
                    if [ "$(cat2 CBC/autoupdates)" == "0" ]
                    then
                         echo "1" > CBC/autoupdates
                         settings_menu
                    elif [ "$(cat2 CBC/autoupdates)" == "1" ]
                    then
                         echo "0" > CBC/autoupdates
                         settings_menu
                    fi
               elif [ "$MENUINPUT" == "n" ] || [ "$MENUINPUT" == "N" ]
               then
                    settings_menu
               else
                    settings_menu #TODO warning
               fi
          fi
          if [ "$(cat2 CBC/autoupdates)" == "0" ]
          then
               if [ "$MENUINPUT" == "2" ]
               then
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Settings - Updates - Autochecks"
                    echo ""
                    if [ "$(cat2 CBC/autochecks)" == "0" ]
                    then
                         echo " Do You want to enable auto checks for updates at start? (y/n)"
                    elif [ "$(cat2 CBC/autochecks)" == "1" ]
                    then
                         echo " Do You want to disable auto checks for updates at start? (y/n)"
                    fi
                    echo ""
                    read -n 1 MENUINPUT
                    if [ "$MENUINPUT" == "y" ] || [ "$MENUINPUT" == "Y" ]
                    then
                         if [ "$(cat2 CBC/autochecks)" == "0" ]
                         then
                              echo "1" > CBC/autochecks
                              settings_menu
                         elif [ "$(cat2 CBC/autochecks)" == "1" ]
                         then
                              echo "0" > CBC/autochecks
                              settings_menu
                         fi
                    elif [ "$MENUINPUT" == "n" ] || [ "$MENUINPUT" == "N" ]
                    then
                         settings_menu
                    else
                         settings_menu #TODO warning
                    fi
               fi
          elif [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
          then
               settings_menu
          else
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
          fi
          ;;
     2)
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Settings- Termux"
          echo ""
          echo " Select option: "
          echo "  1. Autostart with Termux ($AUTOSTART)"
          if [ "$AUTOSTART" == "on" ]
          then
               echo "  2. Exit from termux with CBC exit ($AUTOEXIT)"
          fi
          echo ""
          echo "  b. Return to the main menu"
          echo ""
          read -n 1 MENUINPUT
          if [ "$MENUINPUT" == "1" ]
          then
               clear_output
               echo "   _____ ____   _____  "
               echo "  / ____|  _ \ / ____| "
               echo " | |    | |_) | |      "
               echo " | |    |  _ <| |      "
               echo " | |____| |_) | |____  "
               echo "  \_____|____/ \_____| Settings - Termux - Autostart"
               echo ""
               if [ "$AUTOSTART" == "off" ]
               then
                    echo " Do You want to enable auto CBC start with Termux launch? (y/n)"
                    echo " WARNING! This will clean up Your ~/.bashrc!"
               elif [ "$AUTOSTART" == "on" ]
               then
                    echo " Do You want to disable auto CBC start with Termux launch? (y/n)"
                    echo " WARNING! This will clean up Your ~/.bashrc!"
               fi
               echo ""
               read -n 1 MENUINPUT
               if [ "$MENUINPUT" == "y" ] || [ "$MENUINPUT" == "Y" ]
               then
                    if [ "$AUTOSTART" == "off" ]
                    then
                         echo "cbc" > .bashrc
                         if [ "$AUTOEXIT" == "on" ]
                         then
                              echo "exit" >> .bashrc
                         fi
                         settings_menu
                    elif [ "$AUTOSTART" == "on" ]
                    then
                         echo "" > .bashrc
                         settings_menu
                    fi
               elif [ "$MENUINPUT" == "n" ] || [ "$MENUINPUT" == "N" ]
               then
                    settings_menu
               else
                    settings_menu
               fi
          fi
          if [ "$AUTOSTART" == "on" ]
          then
               if [ "$MENUINPUT" == "2" ]
               then
                    clear_output
                    echo "   _____ ____   _____  "
                    echo "  / ____|  _ \ / ____| "
                    echo " | |    | |_) | |      "
                    echo " | |    |  _ <| |      "
                    echo " | |____| |_) | |____  "
                    echo "  \_____|____/ \_____| Settings - Termux - Autoexit"
                    echo ""
                    if [ "$AUTOEXIT" == "off" ]
                    then
                         echo " Do You want to enable auto termux close when quiting from CBC? (y/n)"
                         echo " WARNING! This will clean up Your ~/.bashrc!"
                    elif [ "$AUTOEXIT" == "on" ]
                    then
                         echo " Do You want to disable auto termux close when quiting from CBC? (y/n)"
                         echo " WARNING! This will clean up Your ~/.bashrc!"
                    fi
                    echo ""
                    read -n 1 MENUINPUT
                    if [ "$MENUINPUT" == "y" ] || [ "$MENUINPUT" == "Y" ]
                    then
                         echo "cbc" > .bashrc
                         if [ "$AUTOEXIT" == "on" ]
                         then
                              echo "" >> .bashrc
                         elif [  "$AUTOEXIT" == "off"  ]
                         then
                              echo "exit" >> .bashrc
                         fi
                         settings_menu
                    elif [ "$MENUINPUT" == "n" ] || [ "$MENUINPUT" == "N" ]
                    then
                         settings_menu
                    else
                         settings_menu
                    fi
               fi
          elif [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
          then
               settings_menu
          else
               clear_output
               echo ""
               echo " ERROR!"
               echo " You have entered an invalid selection!"
               echo " Press any key to return to the main menu."
               echo ""
               read -n 1
               main_menu
          fi
          ;;
     3)
          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| Settings - Loop inte..."
          echo ""
          echo " Interval between loop refresh is set to $LOOPINTERVAL s"
          echo " Value in seconds."
          echo ""
          echo -n " Change value to (or b to return):" && read MENUINPUT
          if [ "$MENUINPUT" == "b" ] || [ "$MENUINPUT" == "B" ]
          then
               settings_menu
          else
               echo "$MENUINPUT" > CBC/loopinterval
               settings_menu
          fi
          ;;
     4)
          clear_output
          echo "--------------------------------------------------------"
          echo "|                         56                           |"
          for i in $(seq 1 13)
          do  
             echo "|                                                      |"
          done
          echo "| 32       This is highly recommended that You      32 |"
          echo "|             will use this size as minimum            |"
          echo "|                  with shown keyboard.                |"
          echo "|          Zoom in, zoom out using two fingers.        |"
          echo "|     Press any key to return to the settings menu.    |"
          for i in $(seq 1 10)
          do  
             echo "|                                                      |"
          done
          echo "|                         56                           |"
          echo -n "-------------------------------------------------------" && read -n 1
          settings_menu
          ;;
     b|B)
          main_menu
          ;;
     *)
          clear_output
          echo ""
          echo " ERROR!"
          echo " You have entered an invalid selection!"
          echo " Press any key to return to the main menu."
          echo ""
          read -n 1
          main_menu
     esac
}

about_menu () {

     clear_output
     echo "   _____ ____   _____  "
     echo "  / ____|  _ \ / ____| "
     echo " | |    | |_) | |      "
     echo " | |    |  _ <| |      "
     echo " | |____| |_) | |____  "
     echo "  \_____|____/ \_____| About"
     echo ""
     echo " Cherry Bash Commander is licenced under GNU GPL v3."
     echo " Tool was created by Wisniew."
     echo " CBC script helps configure and control Cherry kernel."
     echo " Please configure Cherry kernel with this tool only."
     echo " Issues/features requests are only checked at gitlab."
     echo " Thanks for using Cherry projects!"
     echo ""
     echo ""
     echo " Press any key to return."
     echo ""
     read -n 1
     main_menu
}

#
# Start
#

if [ ! "$EUID" -ne 0 ]
then
     if [ ! "$1" == "--ignore-autoupdates" ]
     then
          #
          # Prepering
          #

          rm -rf version_number

          if [ ! -e "CBC" ] # mkdir -p CBC?
          then
               mkdir CBC
          fi

          if [ ! -e "CBC/channel" ]
          then
               touch CBC/channel
          fi

          echo "$CHANNEL" > CBC/channel

          if [ ! -e "CBC/autochecks" ]
          then
               touch CBC/autochecks
               echo "0" > CBC/autochecks
          fi

          if [ ! -e "CBC/autoupdates" ]
          then
               touch CBC/autoupdates
               echo "0" > CBC/autoupdates
          fi

          if [ ! -e "CBC/loopinterval" ]
          then
               touch CBC/loopinterval
               echo "1" > CBC/loopinterval
          fi

          clear_output
          echo "   _____ ____   _____  "
          echo "  / ____|  _ \ / ____| "
          echo " | |    | |_) | |      "
          echo " | |    |  _ <| |      "
          echo " | |____| |_) | |____  "
          echo "  \_____|____/ \_____| "
          echo ""
          echo "Starting..."
          echo ""
          if [ "$(cat2 CBC/autoupdates)" == "1" ]
          then
               case $CHANNEL in
               stable)
                    echo "Checking for updates..."
                    echo ""
                    curl --progress-bar -L -o version_number "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/version_number/version_number.txt?inline=false"
                    NEW_VERSION=$(cat2 version_number)
                    COMPARE=$(awk 'BEGIN {print ("'$NEW_VERSION'" > "'$VERSION'")}')
                    if [ "$COMPARE" == "1" ]
                    then
                         echo ""
                         echo "Updating..."
                         echo ""
                         curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc.sh?inline=false"
                         chmod 755 cbc

                         clear_output
                         echo "   _____ ____   _____  "
                         echo "  / ____|  _ \ / ____| "
                         echo " | |    | |_) | |      "
                         echo " | |    |  _ <| |      "
                         echo " | |____| |_) | |____  "
                         echo "  \_____|____/ \_____| "
                         echo ""
                         echo " CBC updated successfully!"
                         echo ""
                         echo " Press any key to restart CBC"
                         read -n 1
                         bash cbc
                         exit 0
                    fi
                    ;;
               dev)
                    echo ""
                    echo "Updating..."
                    echo ""
                    curl --progress-bar -L -o cbc "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/cbc-dev.sh?inline=false"
                    chmod 755 cbc
                    bash cbc --ignore-autoupdates
                    exit 0
                    ;;
               esac
          else
               if [ "$(cat2 CBC/autochecks)" == "1" ]
               then
                    echo "Checking for updates..."
                    echo ""
                    case $CHANNEL in
                    dev)
                         curl --progress-bar -L -o version_number "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/version_number/version_number_dev.txt?inline=false"
                         ;;
                    stable)
                         curl --progress-bar -L -o version_number "https://gitlab.com/wisniew/cherry-bash-commander/raw/master/version_number/version_number.txt?inline=false"
                         ;;
                    esac
               fi
          fi
     fi
     main_menu
else
     echo ""
     echo "You need ROOT premissions to run CBC!"
     echo ""
fi